<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreaPerubahan extends Model
{
    protected $table = 'area_perubahan';

    public function pdf()
    {
        return $this->hasMany('App\PDFCenter', 'id_area_perubahan');
    }
}
