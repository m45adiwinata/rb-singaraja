<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Auth;
use App\PDFCenter;
use App\Berita;
use App\AreaPerubahan;
use App\RelasiBerita;
use App\DasarHukum;
use App\Profil;
use App\Image;
use App\Roadmap;
use App\Quickwins;
use App\Monev;

class UserController extends Controller
{
    public function index()
    {
        $data['sideaktif'] = 1;
        $data['file'] = 0;
        $data['area'] = AreaPerubahan::get();
        $beritas = Berita::orderBy('id', 'DESC')->where('status','=', 1)->where('id_kategori','!=','2')->limit(5)->get();
        $related = array();
        $data['beritas'] = array();
        foreach($beritas as $berita){
            $relateds = RelasiBerita::where('id_berita','=',$berita->id)->get();
            $relasi_berita = array();
            if(!empty($relateds)){
                foreach($relateds as $related){
                    $relasi_berita = DB::table('relasi_berita')->select('relasi_berita.id_relasi_berita','berita.judul')->join('berita', function($join)
                    {
                        $join->on('relasi_berita.id_relasi_berita', '=', 'berita.id');
                            
                    })->where('relasi_berita.id_berita', '=', $berita->id)->limit(2)->get();
                }
            }
            $data['beritas'][] = array(
                'id'    => $berita->id,
                'judul' => $berita->judul,
                'path'  => $berita->path,
                'penerbit' => $berita->penerbit()->first()->nama,
                'id_kategori' => $berita->id_kategori,
                'created_at' => $berita->created_at,
                'terkaits' => $relasi_berita

            );
        }
        $data['grafis'] = Berita::orderBy('id', 'DESC')->where('status','=', 1)->where('id_kategori','=','2')->limit(5)->get();
        $data['dasarhukums'] = DasarHukum::get();
        $data['populars'] = Berita::inRandomOrder()->where('status','=', 1)->limit(5)->get();



        return view('common.home',$data);
    }

    public function menu(){
        $data['dasarhukums'] = DasarHukum::get();
        return view('components.navbar',$data);
    }

    public function loginForm()
    {
        if(Auth::user()) {
            return redirect('/admin');
        }
        return view('admin.login');
    }

    public function login(Request $request)
    {
        $validated = $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);
        $user = User::where('username', $request->username)->first();
        if($user) {
            if($user->password == md5($request->password)) {
                Auth::login($user);
                return redirect('/admin');
            }
            else {
                return redirect('/login')->with('fail', 'Password not match!');
            }
        }
        
        return redirect('/login')->with('fail', 'Username not registered!');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }

    public function viewPublikasi(){
        $data['sideaktif'] = 7;
        $id = 2;
        $data['publikasi'] = Profil::where('id','=',$id)->first();
        $data['image'] = Image::where('id_parent','=',1)->first();
        $data['dasarhukums'] = DasarHukum::get();
        return view('common.publikasi', $data);
    }

    public function roadmap()
    {
        $data['sideaktif'] = 2.1;
        $data['roadmap'] = Roadmap::first();
        $data['dasarhukums'] = DasarHukum::get();
        return view('common.roadmap', $data);
    }

    public function quickwins()
    {
        $data['sideaktif'] = 2.2;
        $data['quickwins'] = Quickwins::first();
        $data['dasarhukums'] = DasarHukum::get();
        return view('common.quickwins', $data);
    }

    public function monev()
    {
        $data['sideaktif'] = 2.3;
        $data['monev'] = Monev::first();
        $data['dasarhukums'] = DasarHukum::get();
        return view('common.monev', $data);
    }
}
