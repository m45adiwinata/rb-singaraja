<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\PDFCenter;
use App\PDFDasarHukum;
use App\AreaPerubahan;
use App\DasarHukum;
use App\TimRB;
use App\KategoriBerita;
use App\Berita;
use App\Penerbit;
use App\RelasiBerita;
use App\User;
use App\Profil;
use App\Image;
use App\Roadmap;
use App\Quickwins;
use App\Monev;
use Session;
use File;
use URL;
use Validator;
class AdminController extends Controller
{
    public function index()
    {
        if(!Auth::user()) {
            return redirect('login');
        }
        $data['count_users_all'] = count(User::all());
        $data['count_kategori_all'] = count(KategoriBerita::all());
        $data['count_users_all'] = count(User::all());
        $data['count_laporan_all'] = count(PDFCenter::all());
        $data['count_berita_all'] = count(Berita::all());

        $data['beritas'] = Berita::where('status','=','1')->orderBy('counter', 'DESC')->limit(4)->get();

        $area1 = PDFCenter::where('id_area_perubahan','=',1)->get();
        if(!empty($area1)){
            $data['area1'] = count($area1);
        }else{
            $data['area1'] = 0;
        }
        $area2 = PDFCenter::where('id_area_perubahan','=',2)->get();
        if(!empty($area2)){
            $data['area2'] = count($area2);
        }else{
            $data['area2'] = 0;
        }
        $area1 = PDFCenter::where('id_area_perubahan','=',3)->get();
        if(!empty($area3)){
            $data['area3'] = count($area3);
        }else{
            $data['area3'] = 0;
        }
        $area4 = PDFCenter::where('id_area_perubahan','=',4)->get();
        if(!empty($area4)){
            $data['area4'] = count($area4);
        }else{
            $data['area4'] = 0;
        }
        $area5 = PDFCenter::where('id_area_perubahan','=',5)->get();
        if(!empty($area5)){
            $data['area5'] = count($area5);
        }else{
            $data['area5'] = 0;
        }
        $area6 = PDFCenter::where('id_area_perubahan','=',6)->get();
        if(!empty($area1)){
            $data['area6'] = count($area6);
        }else{
            $data['area6'] = 0;
        }
        $area7 = PDFCenter::where('id_area_perubahan','=',7)->get();
        if(!empty($area7)){
            $data['area7'] = count($area7);
        }else{
            $data['area7'] = 0;
        }
        $area8 = PDFCenter::where('id_area_perubahan','=',8)->get();
        if(!empty($area8)){
            $data['area8'] = count($area8);
        }else{
            $data['area8'] = 0;
        }
        $data['sideaktif'] = 6;
        return view('admin.contents.dashboard',$data);
    }

    public function pdfCenter()
    {
        if(!Auth::user()) {
            return redirect('login');
        }
        $data['sideaktif'] = 2;
        $data['pdfs'] = PDFCenter::get();
        return view('admin.contents.pdfcenter', $data);
    }

    public function getPDFDetail()
    {
        if(!Auth::user()) {
            return redirect('login');
        }
        $data = PDFCenter::find($_GET['id_file']);

        return $data;
    }

    public function formTambahPDF()
    {
        if(!Auth::user()) {
            return redirect('login');
        }
        $data['sideaktif'] = 2;
        $data['area_perubahans'] = AreaPerubahan::get();
        return view('admin.pdf.formtambah',$data);
    }

    public function tambahPDF(Request $request)
    {
        $validated = $request->validate([
            'id_area_perubahan' => 'required',
            'title' => 'required',
            'description' => 'required',
            'file' => 'required',
        ]);
        $file = $request->file('file');
        $nama_file = time()."-".$file->getClientOriginalName();
        $file->move('pdf',$nama_file);
        $data = new PDFCenter;
        if(isset($request->id_area_perubahan)){
            if($request->id_area_perubahan == '-'){
                $request->id_area_perubahan = null;
            }
        }
        $data->id_area_perubahan = $request->id_area_perubahan;
        $data->title_file = $request->title;
        $data->tagline = $request->tagline;
        $data->description = $request->description;
        $data->path = "pdf/".$nama_file;
        $data->save();
        Session::flash('success', 'Laporan berhasil ditambahkan!');

        return redirect('/admin/pdf');
    }

    public function formEditPDF($id)
    {
        if(!Auth::user()) {
            return redirect('login');
        }
        $data['pdf'] = PDFCenter::find($id);
        $data['area_perubahans'] = AreaPerubahan::get();
        $data['sideaktif'] = 2;
        return view('admin.pdf.formedit', $data);
    }

    public function editPDF(Request $request, $id)
    {

        $data = PDFCenter::find($id);
        if(isset($request->file)) {
            if(file_exists($data->path)){
                $image_path = public_path($data->path);
                unlink($data->path);
                if(File::exists($image_path)) {
                    File::delete($image_path);
                }
            }
            $file = $request->file('file');
            $nama_file = time()."-".$file->getClientOriginalName();
            $file->move('pdf',$nama_file);
            $data->path = "pdf/".$nama_file;
        }
        if(isset($request->id_area_perubahan)){
            $data->id_area_perubahan = $request->id_area_perubahan;
        }
        if(isset($request->title)){
            $data->title_file = $request->title;
        }
        if(isset($request->tagline)){
            $data->tagline = $request->tagline;
        }
        if(isset($request->description)){
            $data->description = $request->description;
        }
        $data->save();
        Session::flash('success', 'Laporan berhasil diupdate!');
        return redirect('/admin/pdf');
    }

    public function deletePDF(Request $request)
    {
        $validated = $request->validate([
            'id' => 'required',
        ]);
        $data = PDFCenter::find($request->id);
        $image_path = public_path($data->path);
        if(File::exists($image_path)) {
            File::delete($image_path);
        }
        $data->delete();
        Session::flash('success', 'Laporan berhasil dihapus!');
        return redirect('/admin/pdf');
    }

    public function dasarHukum(){
        if(!Auth::user()) {
            return redirect('login');
        }
        $data['sideaktif'] = 1;
        $data['dasarhukums'] = DasarHukum::get();
        return view('admin.contents.dasarhukum', $data);
    }

    public function tambahDasarHukumForm(){
        if(!Auth::user()) {
            return redirect('login');
        }
        $data['sideaktif'] = 1;
        return view('admin.contents.tambahdasarhukum', $data);
    }

    public function tambahDasarHukum(Request $request){
        $validated = $request->validate([
            'judul' => 'required',
        ]);
        $data = new DasarHukum;
        $data->nama = $request->judul;
        if(isset($request->isi)){
            $data->isi = $request->isi;
        }else{
            $data->isi = '';
        }
        $data->path = '';
    
        if($data->save()){
            $last_item = DasarHukum::orderBy('id', 'DESC')->limit(1)->get();
            if($last_item[0]->id){
                if(isset($request->file)){
                    foreach($request->file('file') as $file){
                        $datas = new PDFDasarHukum;
                        $datas->id_dasar_hukum = $last_item[0]->id;
                        $filename = $file->getClientOriginalName();
                        $nama_file = time()."-".$file->getClientOriginalName();

                        $file->move('dasarhukum',$nama_file);
                        $datas->path = "dasarhukum/".$nama_file;
                        print_r($filename);
                        $datas->save();
                    }
                }
            }
            Session::flash('success', 'Dasar Hukum berhasil ditambhahkan!');
        }else{
            Session::flash('danger', 'Dasar Hukum gagal ditambahkan!');
        }
        
        return redirect('/admin/profil/dasar-hukum');
    }

    public function detailDasarHukum($id){
        if(!Auth::user()) {
            return redirect('login');
        }
        $data['base'] = URL::to('/');
        if(isset($id)){
            $data['dasarhukum'] = DasarHukum::find($id);
            $data['files'] = PDFDasarHukum::where('id_dasar_hukum',$id)->get();
            return view('admin.contents.editdasarhukum', $data);
        }
    }

    public function editDasarHukum(Request $request){
        $validator = Validator::make($request->all(),[
            'id' => 'required',
        ]);

        $data = DasarHukum::find($request->id);
        if(isset($request->judul)){
            $data->nama = $request->judul;
        }
        if(isset($request->isi)){
            $data->isi = $request->isi;
        }
        if(isset($request->file)) {
            if(file_exists($data->path)){
                $image_path = public_path($data->path);
                unlink($data->path);
                if(File::exists($image_path)) {
                    File::delete($image_path);
                }
            }
            $file = $request->file('file');
            $nama_file = time()."-".$file->getClientOriginalName();
            $file->move('dasarhukum',$nama_file);
            $data->path = "dasarhukum/".$nama_file;
        }
        $data->save();
        Session::flash('success', 'Dasar Hukum berhasil di update!');
        return redirect('/admin/profil/dasar-hukum/'.$request->id);
    }

    public function editDasarHukumPDF(Request $request){
        $validator = Validator::make($request->all(),[
            'id' => 'required',
            'file' => 'required|max:200',
        ]);

        $data = PDFDasarHukum::find($request->id);
     
        if(isset($request->file)) {
            if(file_exists($data->path)){
                $image_path = public_path($data->path);
                unlink($data->path);
                if(File::exists($image_path)) {
                    File::delete($image_path);
                }
            }
            $file = $request->file('file');
            $nama_file = time()."-".$file->getClientOriginalName();
            $file->move('dasarhukum',$nama_file);
            $data->path = "dasarhukum/".$nama_file;
        }
        $data->save();
        Session::flash('success', 'Dasar Hukum berhasil di update!');
        return redirect('/admin/profil/dasar-hukum/'.$request->id_ds);
    }

    public function deleteDasarHukum(Request $request){
        $validated = $request->validate([
            'id' => 'required',
        ]);
        $data = DasarHukum::find($request->id);
        if(file_exists($data->path)){
            unlink($data->path);
        }
        $data->delete();
        Session::flash('success', 'Dasar Hukum berhasil dihapus!');
        return redirect('/admin/profil/dasar-hukum');
    }

    public function timRB(){
        if(!Auth::user()) {
            return redirect('login');
        }
        $data['sideaktif'] = 7;
        $data['timrb'] = TimRB::get()->first();
        return view('admin.contents.timRB', $data);
    }

    public function editTimRB(Request $request){
        $id = 1;
        $data = TimRB::find($id);
        if(isset($request->file)) {
            if(file_exists($data->path)){
                unlink($data->path);
            }
            $file = $request->file('file');
            $nama_file = time()."-".$file->getClientOriginalName();
            $file->move('pdf',$nama_file);
            $data->path = "pdf/".$nama_file;
        }
        $data->description = $request->description;
        $data->save();
        return redirect('/admin/tim-rb');
    }
    public function MasterKategori(){
        if(!Auth::user()) {
            return redirect('login');
        }
        $data['sideaktif'] = 5;
        $data['categories'] = KategoriBerita::get();
        return view('admin.master.kategoriberita', $data);
    }

    public function MasterAddKategori(Request $request){
        if(!Auth::user()) {
            return redirect('login');
        }
        $validated = $request->validate([
            'nama' => 'required',
        ]);
        $data = new KategoriBerita;
        $data->nama = $request->nama;
        $data->description = $request->description;
        $data->save();
        Session::flash('success', 'Kategori berhasil ditambahkan!');
        return redirect('/admin/master/kategori-berita');
    }

    public function MasterEditKategori(Request $request){
        if(!Auth::user()) {
            return redirect('login');
        }
        $validated = $request->validate([
            'id'    => 'required',
            'nama' => 'required',
        ]);
        $data = KategoriBerita::find($request->id);
        $data->nama = $request->nama;
        $data->description = $request->description;
        $data->save();
        Session::flash('success', 'Kategori berhasil diupdate!');
        return redirect('/admin/master/kategori-berita');
        
    }

    public function deleteMasterKategori(Request $request)
    {
        $validated = $request->validate([
            'id' => 'required',
        ]);
        $berita = Berita::where('id_kategori','=',$request->id)->get();
        if(empty($berita)){
            $data = KategoriBerita::find($request->id);
            $data->delete();
            Session::flash('success', 'Kategori berhasil didelete!');
        }else{
            Session::flash('danger', 'Kategori tidak berhasil dihapus karena masih ada berita yang berelasi!');
        }

        return redirect('/admin/master/kategori-berita');
    }

    public function daftarBerita()
    {
        if(!Auth::user()) {
            return redirect('login');
        }
        if(isset($_GET['search'])) {
            $data['var_search'] = $_GET['search'];
            $data['beritas'] = Berita::where('judul', 'like', '%'.$_GET['search'].'%')->orderBy('id', 'DESC')->paginate(5)->appends(request()->query());
        }else{
            $data['beritas'] = Berita::orderBy('id', 'DESC')->paginate(5);
        }
        $data['sideaktif'] = 3;
        return view('admin.berita.daftarberita',$data);
    }


    public function tambahBeritaForm()
    {
        if(!Auth::user()) {
            return redirect('login');
        }
        $data['categories'] = KategoriBerita::get();
        $data['penerbits'] = Penerbit::get();
        $data['sideaktif'] = 3;
        $data['beritas'] = Berita::get();

        return view('admin.berita.tambahberita',$data);
    }

    public function tambahBerita(Request $request){
        $validated = $request->validate([
            'judul' => 'required',
            'id_kategori' => 'required',
            'deskripsi' => 'required',
            'file' => 'required',
            'status' => 'required'
        ]);
        $file = $request->file('file');
        $nama_file = time()."-".$file->getClientOriginalName();
        $file->move(public_path().'/image_berita',$nama_file);
        $data = new Berita;
        $data->id_kategori = $request->id_kategori;
        
        if(isset($request->judul)){
            $data->judul = $request->judul;
        }else{
            $data->judul = '';
        }

        if(isset($request->kutipan)){
            $data->kutipan = $request->kutipan;
        }else{
            $data->kutipan = '';
        }

        if(isset($request->deskripsi)){
            $data->deskripsi = $request->deskripsi;
        }else{
            $data->deskripsi = '';
        }

        if(isset($request->status)){
            $data->status = $request->status;
        }else{
            $data->status = 1;
        }
        

        $data->id_penerbit = $request->id_penerbit;
        $data->path = "image_berita/".$nama_file;
        $data->save();
        $id_berita = $data->id;
        if(isset($request->relasi)){
            foreach($request->relasi as $key => $relasi_id){
                $data = new RelasiBerita;
                $data->id_berita = $id_berita;
                $data->id_relasi_berita = $relasi_id;
                $data->save();
            }
        }
        
        Session::flash('success', 'Berita berhasil ditambahkan!');
        return redirect('/admin/berita/daftar-berita');
    }

    public function editBeritaForm($id)
    {
        if(!Auth::user()) {
            return redirect('login');
        }
        $data['berita'] = Berita::find($id);
        $data['beritas'] = Berita::get();
        $data['categories'] = KategoriBerita::get();
        $data['penerbits'] = Penerbit::get();

        $data['relasi'] = RelasiBerita::where('id_berita','=',$id)->get();
        
        $data['sideaktif'] = 3;
        return view('admin.berita.editberita', $data);
    }

    public function editBerita(Request $request){
        $validated = $request->validate([
            'id' => 'required',
        ]);
        $data = Berita::find($request->id);
        if(isset($request->file)) {
            if(file_exists(public_path()."/".$data->path)){
                unlink(public_path()."/".$data->path);
            }
            $file = $request->file('file');
            $nama_file = time()."-".$file->getClientOriginalName();
            $file->move(public_path().'/image_berita',$nama_file);
            $data->path = "image_berita/".$nama_file;
        }
        $data->id_kategori = $request->id_kategori;

        if(isset($request->judul)){
            $data->judul = $request->judul;
        }

        if(isset($request->kutipan)){
            $data->kutipan = $request->kutipan;
        }

        if(isset($request->deskripsi)){
            $data->deskripsi = $request->deskripsi;
        }
        
        if(isset($request->status)){
            $data->status = $request->status;
        }

        if(isset($request->id_penerbit)){
            $data->id_penerbit = $request->id_penerbit;
        }
        if(isset($request->relasi)){
            foreach($request->relasi as $key => $relasi_id){
                $result = RelasiBerita::where('id_relasi_berita','=',$relasi_id)->where('id_berita','=',$request->id)->get();
                if(count($result) == 0){
                    $data = new RelasiBerita;
                    $data->id_berita = $request->id;
                    $data->id_relasi_berita = $relasi_id;
                    $data->save();
                }

            }
        }else if($request->relasi == null){
            $relasi = RelasiBerita::where('id_berita','=',$request->id)->get();
            if(!empty($relasi)){
                foreach($relasi as $data){
                    $data::where('id_berita','=',$request->id)->delete();
                }
            }
        }
        Session::flash('success', 'Berita berhasil diupdate!');
        $data->save();
        return redirect('/admin/berita/daftar-berita');
    }

    public function deleteBerita(Request $request)
    {
        $validated = $request->validate([
            'id' => 'required',
        ]);

        $relasi = RelasiBerita::where('id_relasi_berita','=',$request->id)->get();
        if(!empty($relasi)){
            foreach($relasi as $data){
                $data::where('id_relasi_berita','=',$request->id)->delete();
            }
        }

        $relasi = RelasiBerita::where('id_berita','=',$request->id)->get();
        if(!empty($relasi)){
            foreach($relasi as $data){
                $data::where('id_berita','=',$request->id)->delete();
            }
        }

        $data = Berita::find($request->id);
        $data->delete();

        Session::flash('success', 'Berita berhasil didelete!');
        return redirect('/admin/berita/daftar-berita');
    }

    public function MasterUser(){
        if(!Auth::user()) {
            return redirect('login');
        }
        $data['users'] = User::get();
        $data['sideaktif'] = 9;
        return view('admin.master.user', $data);
    }

    public function MasterAddUser(Request $request){
        if(!Auth::user()) {
            return redirect('login');
        }
        $validated = $request->validate([
            'nama' => 'required',
            'username' => 'required',
            'password' => 'required',
        ]);
        $data = new User;
        $data->name = $request->nama;
        $data->username = $request->username;
        $data->password = md5($request->password);
        $data->email = $request->username.'@email.com';
        $data->save();
        Session::flash('success', 'User berhasil ditambahkan!');
        return redirect('/admin/master/users');
    }

    public function MasterEditUser(Request $request)
    {
        if(!Auth::user()) {
            return redirect('login');
        }
        $validated = $request->validate([
            'id' => 'required',
        ]);
        $data = User::find($request->id);
        $data->name = $request->nama;
        $data->username = $request->username;
        if(isset($request->password)) {
            $data->password = md5($request->password);
        }
        $data->email = $request->username.'@email.com';
        $data->save();
        Session::flash('success', 'User berhasil diupdate!');
        return redirect('/admin/master/users');
    }

    public function MasterdeleteUser(Request $request)
    {
        if(!Auth::user()) {
            return redirect('login');
        }
        $validated = $request->validate([
            'id' => 'required',
        ]);   

        User::find($request->id)->delete();
        Session::flash('success', 'User berhasil dihapus!');
        return redirect('/admin/master/users');
    }

    public function Publikasi(){
        
        if(!Auth::user()) {
            return redirect('login');
        }
        $data['sideaktif'] = 8;
        $id = 2;
        $data['publikasi'] = Profil::where('id','=',$id)->first();
        $data['image'] = Image::where('id_parent','=',1)->first();
        return view('admin.contents.publikasi', $data);
    }

    public function editPublikasi(Request $request){
        $id = 2;
        $data = Profil::find($id);
        if(isset($request->judul)){
            $data->judul = $request->judul;
        }
        if(isset($request->konten)){
            $data->konten = $request->konten;
        }
        $data->save();

        if(isset($request->file)){
            $id_parent = 1;
            $image = Image::where('id_parent','=',$id_parent)->first();
            // if(empty(file_exists($image->path))){
            //     unlink($image->path);
            // }
            $file = $request->file('file');
            $nama_file = time()."-".$file->getClientOriginalName();
            $file->move('publikasi',$nama_file);
            $image->path = "publikasi/".$nama_file;
            $image->save();
        }
        Session::flash('success', 'Berita berhasil ditambahkan!');
        return redirect('/admin/publikasi');
    }

    public function roadmap()
    {
        if(!Auth::user()) {
            return redirect('login');
        }
        $data['sideaktif'] = 3.1;
        $data['roadmap'] = Roadmap::get()->first();
        return view('admin.contents.roadmap', $data);
    }
    
    public function roadmapEdit(Request $request)
    {
        if(!Auth::user()) {
            return redirect('login');
        }
        $validated = $request->validate([
            'roadmap' => 'required',
        ]);
        $data = Roadmap::first();
        if(!$data) {
            $data = new Roadmap;
        }
        if(isset($request->file)) {
            if(file_exists($data->path)){
                unlink($data->path);
            }
            $file = $request->file('file');
            $nama_file = time()."-".$file->getClientOriginalName();
            $file->move('roadmapPDF',$nama_file);
            $data->path = "roadmapPDF/".$nama_file;
        }
        $data->roadmap = $request->roadmap;
        $data->save();
        
        return redirect('/admin/roadmap-rb');
    }

    public function quickwins()
    {
        if(!Auth::user()) {
            return redirect('login');
        }
        $data['sideaktif'] = 3.2;
        $data['quickwins'] = Quickwins::get()->first();
        return view('admin.contents.quickwins', $data);
    }
    
    public function quickwinsEdit(Request $request)
    {
        if(!Auth::user()) {
            return redirect('login');
        }
        $data = Quickwins::first();
        if(!$data) {
            $data = new Quickwins;
        }
        if(isset($request->file)) {
            if(file_exists($data->path)){
                unlink($data->path);
            }
            $file = $request->file('file');
            $nama_file = time()."-".$file->getClientOriginalName();
            $file->move('quickwinsPDF',$nama_file);
            $data->path = "quickwinsPDF/".$nama_file;
            $data->save();
        }
        
        return redirect('/admin/quickwins');
    }

    public function monev()
    {
        if(!Auth::user()) {
            return redirect('login');
        }
        $data['sideaktif'] = 3.3;
        $data['monev'] = Monev::get()->first();
        return view('admin.contents.monev', $data);
    }
    
    public function monevEdit(Request $request)
    {
        if(!Auth::user()) {
            return redirect('login');
        }
        $validated = $request->validate([
            'monev' => 'required',
        ]);
        $data = Monev::first();
        if(!$data)
        {
            $data = new Monev;
        }
        $data->monev = $request->monev;
        $data->save();
        
        return redirect('/admin/monev-rb');
    }


    
}
