<?php

namespace App\Http\Controllers;
use App\Berita;
use App\DasarHukum;
use Illuminate\Http\Request;
use File;

class BeritaController extends Controller
{
    public function index()
    {
        if(isset($_GET['fill'])) {
            if($_GET['fill'] == 'bagian-organisasi'){
                if(isset($_GET['input_search'])) {
                    $data['beritas'] = Berita::where('id_penerbit', '=', '1')->where('status','=', 1)->where('judul', 'like', '%'.$_GET['input_search'].'%')->orderBy('id', 'DESC')->paginate(6);
                }
                else {
                    $data['beritas'] = Berita::where('id_penerbit', '=', '1')->where('status','=', 1)->orderBy('id', 'DESC')->paginate(6);
                }
            }elseif($_GET['fill'] == 'kominfo-buleleng'){
                if(isset($_GET['input_search'])) {
                    $data['beritas'] = Berita::where('id_penerbit', '=', '2')->where('status','=', 1)->where('judul', 'like', '%'.$_GET['input_search'].'%')->orderBy('id', 'DESC')->paginate(6);
                }
                else {
                    $data['beritas'] = Berita::where('id_penerbit', '=', '2')->where('status','=', 1)->orderBy('id', 'DESC')->paginate(6);
                }
            }else{
                if(isset($_GET['input_search'])) {
                    $data['beritas'] = Berita::orderBy('id', 'DESC')->where('status','=', 1)->where('judul', 'like', '%'.$_GET['input_search'].'%')->paginate(6);
                }
                else {
                    $data['beritas'] = Berita::orderBy('id', 'DESC')->where('status','=', 1)->paginate(6);
                }
            }
        }else{
            if(isset($_GET['input_search'])) {
                $data['beritas'] = Berita::orderBy('id', 'DESC')->where('status','=', 1)->where('judul', 'like', '%'.$_GET['input_search'].'%')->paginate(6);
            }
            else {
                $data['beritas'] = Berita::orderBy('id', 'DESC')->where('status','=', 1)->paginate(6);
            }
        }

        $data['dasarhukums'] = DasarHukum::get();

        $data['sideaktif'] = 6;
        return view('berita.index',$data);
    }
    public function detailBerita($id){
        $data['berita'] = Berita::find($id);

        $data['title'] = $data['berita']->judul;
        $data['description'] = $data['berita']->kutipan;
        $data['image'] = url('/').'/'.$data['berita']->path;
        $data['url'] = url('/').'/berita/'.$data['berita']->id.'/'.'detailberita';

        $data['sideaktif'] = 6;
        $data['berita']->counter = $data['berita']->counter + 1;
        $data['berita']->save();

        $data['dasarhukums'] = DasarHukum::get();

        return view('berita.detailberita',$data);
    }
    public function cariBerita()
    {
        $response = array();
        if(isset($_GET['judul_berita'])) {
            $beritas = Berita::where('judul', 'like', '%'.$_GET['judul_berita'].'%')->limit(5)->get(['id','judul']);
            foreach($beritas as $berita){
                $response[] = array("value"=>$berita->id, "label"=>$berita->judul);
            }
        }
        else {
            $response[] = array("value"=>"", "label"=>"");
        }
        return response()->json($response);
    }

    public function updateFileDasarHukum()
    {
        $response = array();
        // if(isset($_GET['id'])) {
        //     $data = PDFDasarHukum::find($_GET['id']);
        //     if(isset($_GET['files_edit'])) {
        //         if(file_exists($data->path)){
        //             $image_path = public_path($data->path);
        //             unlink($data->path);
        //             if(File::exists($image_path)) {
        //                 File::delete($image_path);
        //             }
        //         }
        //         $file = $_GET['files_edit'];
        //         $nama_file = time()."-".$file->getClientOriginalName();
        //         $file->move('dasarhukum',$nama_file);
        //         $data->path = "dasarhukum/".$nama_file;
        //     }
        //     $data->save();
        //     $response[] = array("response_code"=>200, "message"=>"File Berhasil Diupdate!");
        // }
        // else {
        //     $response[] = array("value"=>"", "label"=>"");
        // }
        // return response()->json($response);
    }
}