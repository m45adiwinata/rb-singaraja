<?php

namespace App\Http\Controllers;
use App\PDFCenter;
use App\AreaPerubahan;
use App\DasarHukum;
use App\TimRB;
use URL;
use File;

class BirokrasiController extends Controller
{
    public function index()
    {

    }

    public function detailLaporan($id){
        $data['sideaktif'] = 4;
        $data['biroaktif'] = $id;
        $area = AreaPerubahan::find($id);
        $data['pdfs'] = $area->pdf()->get();
        $data['base_url'] = URL::to('/');
        $data['area_perubahan'] = $area;

        $data['dasarhukums'] = DasarHukum::get();

        return view('pdf.manajemen', $data);
    }
    public function viewTimRB(){
        $data['sideaktif'] = 3;
        $id = 1;
        $data['tim_rb'] = TimRB::find($id);

        $data['dasarhukums'] = DasarHukum::get();

        return view('common.timrb', $data);
    }
}