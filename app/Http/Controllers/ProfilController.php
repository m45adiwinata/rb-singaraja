<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\PDFCenter;
use App\Profil;
use App\DasarHukum;
use URL;
use Session;
use File;

class ProfilController extends Controller
{
    public function index()
    {
       
    }
    public function latarBelakang(){
        if(!Auth::user()) {
            return redirect('login');
        }
        $data['sideaktif'] = 1;
        $id = 1;
        $data['latarbelakang'] = Profil::find($id);
        return view('admin.contents.latarbelakang', $data);
    }

    public function editLatarBelakang(Request $request){
        $validated = $request->validate([
            'judul' => 'required',
            'konten' => 'required',
        ]);
        $id = 1;
        $data = Profil::find($id);
        $data->judul = $request->judul;
        $data->konten = $request->konten;
        $data->save();
        Session::flash('success', 'Latar Belakang berhasil di update!');
        return redirect('/admin/profil/latar-belakang');
    }

    public function viewlatarBelakang(){
        $data['sideaktif'] = 2;
        $data['profilaktif'] = 2;
        $id = 1;
        $latarbelakang = Profil::find($id);
        $data['judul'] = $latarbelakang['judul'];
        $data['konten'] =  str_replace('"','',html_entity_decode($latarbelakang['konten'], ENT_QUOTES, 'UTF-8'));

        $data['dasarhukums'] = DasarHukum::get();

        return view('common.latarbelakang', $data);
    }
    public function viewDasarHukum($id){
        $data['sideaktif'] = 2;
        // $data['profilaktif'] = 2;
        $data['dasarhukum'] = DasarHukum::find($id);

        $data['dasarhukums'] = DasarHukum::get();

        return view('common.dasarhukum', $data);
    }
}