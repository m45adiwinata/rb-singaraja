<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PDFCenter extends Model
{
    protected $table = 'pdf_files';
}
