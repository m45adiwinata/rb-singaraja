<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table = 'berita';

    public function kategori()
    {
        return $this->belongsTo('App\KategoriBerita', 'id_kategori');
    }

    public function penerbit()
    {
        return $this->belongsTo('App\Penerbit', 'id_penerbit');
    }

    public function relatedNews()
    {
        return $this->hasMany('App\RelasiBerita', 'id_berita');
    }

}
