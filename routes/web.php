<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'UserController@index');
Route::get('/login', 'UserController@loginForm');
Route::post('/login', 'UserController@login')->name('login');
Route::get('/logout', 'UserController@logout');

Route::get('/admin', 'AdminController@index');
Route::get('/admin/pdf', 'AdminController@pdfCenter');
Route::get('/admin/pdf/tambah', 'AdminController@formTambahPDF');
Route::post('/admin/pdf/tambah', 'AdminController@tambahPDF')->name('tambah-pdf');
Route::get('/admin/pdf/{id}/edit', 'AdminController@formEditPDF');
Route::post('/admin/pdf/{id}/edit', 'AdminController@editPDF')->name('edit-pdf');
Route::post('/admin/pdf/delete', 'AdminController@deletePDF')->name('delete-pdf');

Route::get('/birokrasi/{id}', 'BirokrasiController@detailLaporan');

Route::get('/berita', 'BeritaController@index');
Route::get('/berita/{id}/detailberita', 'BeritaController@detailBerita');

Route::get('/admin/profil/latar-belakang', 'ProfilController@latarBelakang');
Route::post('/admin/profil/edit-latar-belakang', 'ProfilController@editLatarBelakang')->name('editlatarbelakang');

Route::get('/profil/latar-belakang', 'ProfilController@viewLatarBelakang');

Route::get('/publikasii', 'UserController@viewPublikasi');

Route::get('/admin/profil/dasar-hukum', 'AdminController@dasarHukum');
Route::get('/admin/profil/dasar-hukum/tambah', 'AdminController@tambahDasarHukumForm');
Route::post('/admin/profil/dasar-hukum/tambah-dasar-hukum', 'AdminController@tambahDasarHukum')->name('tambah-dasarhukum');
Route::get('/admin/profil/dasar-hukum/{id}', 'AdminController@detailDasarHukum');
Route::post('/admin/profil/edit-dasarhukum', 'AdminController@editDasarHukum')->name('edit-dasarhukum');
Route::post('/admin/profil/edit-dasarhukum-pdf', 'AdminController@editDasarHukumPDF')->name('edit-dasarhukum-pdf');
Route::post('/admin/profil/dasar-hukum/delete', 'AdminController@deleteDasarHukum')->name('delete-dasarhukum');

Route::get('/admin/roadmap-rb', 'AdminController@roadmap');
Route::post('/admin/roadmap-rb', 'AdminController@roadmapEdit')->name('roadmap-edit');
Route::get('/admin/quickwins', 'AdminController@quickwins');
Route::post('/admin/quickwins', 'AdminController@quickwinsEdit')->name('quickwins-edit');
Route::get('/admin/monev-rb', 'AdminController@monev');
Route::post('/admin/monev-rb', 'AdminController@monevEdit')->name('monev-edit');

Route::get('/profil/dasar-hukum/{id}', 'ProfilController@viewDasarHukum');

Route::get('/admin/tim-rb', 'AdminController@timRB');
Route::post('/admin/edit-timrb', 'AdminController@editTimRB')->name('edit-timrb');

Route::get('/tim-rb', 'BirokrasiController@viewTimRB');

Route::get('/admin/master/kategori-berita', 'AdminController@MasterKategori');
Route::post('/admin/master/kategori-berita/add', 'AdminController@MasterAddKategori')->name('add-kategori');
Route::post('/admin/master/kategori-berita/edit', 'AdminController@MasterEditKategori')->name('edit-kategori');
Route::post('/admin/master/kategori-berita/delete', 'AdminController@deleteMasterKategori')->name('delete-kategori');


Route::get('/admin/berita/daftar-berita', 'AdminController@daftarBerita')->name('daftar-berita');

Route::get('/admin/berita/tambah-form', 'AdminController@tambahBeritaForm');
Route::post('/admin/berita/tambah', 'AdminController@tambahBerita')->name('tambah-berita');
Route::get('/admin/berita/{id}/edit', 'AdminController@editBeritaForm');
Route::post('/admin/berita/edit', 'AdminController@editBerita')->name('edit-berita');
Route::post('/admin/berita/delete', 'AdminController@deleteBerita')->name('delete-berita');

Route::get('/admin/master/users', 'AdminController@MasterUser');
Route::post('/admin/master/users/add', 'AdminController@MasterAddUser')->name('add-user');
Route::post('/admin/master/users/edit', 'AdminController@MasterEditUser')->name('edit-user');
Route::post('/admin/master/users/delete', 'AdminController@MasterDeleteUser')->name('delete-user');

Route::get('/admin/publikasi', 'AdminController@Publikasi');
Route::post('/admin/publikasi/edit', 'AdminController@editPublikasi')->name('editpublikasi');

Route::get('/roadmap-rb', 'UserController@roadmap');
Route::get('/quickwins', 'UserController@quickwins');
Route::get('/monev-rb', 'UserController@monev');
