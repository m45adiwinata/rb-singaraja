<!DOCTYPE html>
<html>

	<head>

		<title>rb.bulelengkab</title>

		
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta content="IE=edge" http-equiv="X-UA-Compatible">
		<meta name="x-apple-disable-message-reformatting">
		<meta name="viewport" content="width=device-width, initial-scale=0.86, maximum-scale=3.0, minimum-scale=0.86">

		<!-- <link rel="stylesheet" type="text/css" href="style/fonts/style.css"> -->
		<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    	<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
		<!-- CSS only -->
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
		<link rel="stylesheet" href="{{asset('css/bootstrap-4.min.css')}}">
		<link rel="stylesheet" href="{{asset('css/jquery-ui.min.css')}}">
		<link rel="stylesheet" href="{{asset('css/owlcarousel/owl.carousel.min.css')}}">
		<link rel="stylesheet" href="{{asset('css/owlcarousel/owl.theme.default.css')}}">
		<link rel="stylesheet" href="{{asset('css/owlcarousel/owl.theme.default.css')}}">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
  		<link rel="stylesheet" href="{{asset('css/style.css')}}">
  		<link rel="stylesheet" href="{{asset('css/pelayanan.css')}}">
  		<link rel="stylesheet" href="{{asset('css/bvselect.css')}}">
  		<link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}"  type='image/x-icon'>
		@if(isset($url))
			<meta property="og:url" content="{{$url}}" />
		@else
			<meta property="og:url" content="https://rb.bulelengkab.go.id" />
		@endif
		<meta property="og:type" content="website" />
		@if(isset($title))
			<meta property="og:title" content="{{$title}}" />
		@else
			<meta property="og:title" content="Website Reformasi Birokrasi" />
		@endif
		@if(isset($description))
			<meta property="og:description" content="{{$description}}" />
		@else
			<meta property="og:description" content="Reformasi Birokrasi" />
		@endif
		@if(isset($image))
			<meta property="og:image" content="{{$image}}" />
			<meta property="og:image:width" content="400">
			<meta property="og:image:height" content="200">
		@else
			<meta property="og:image" content="{{asset('img/sgrjlogo.png')}}" />
			<meta property="og:image:width" content="400">
			<meta property="og:image:height" content="200">
		@endif

		@yield('style')
	</head>

	<body>

		@include('components.navbar')
		<main class="content-wrapper">
			@yield('content')
		</main>	
		@include('components.footer')
		<!-- jQuery -->
		<script src="{{asset('js/jquery/jquery.min.js')}}"></script>

		<!-- jQuery UI -->
		<script src="{{asset('js/jquery-ui/jquery-ui.min.js')}}"></script>

		<!-- bootstrap -->
		<script src="{{asset('js/bootstrap/bootstrap.min.js')}}"></script>
		<script src="{{asset('js/owlcarousel/owl.carousel.min.js')}}"></script>
		<script src="{{asset('js/bvselect.js')}}"></script>
		<script>

			$(document).ready(function(){
				$(".owl-pelayanan-birokrasi").owlCarousel({
					loop:true,
					margin:10,
					nav:true,
					dots:false,
					responsive:{
						0:{
							items:2
						},
						600:{
							items:4
						},
						1000:{
							items:4
						}
					}
				});
				$(".owl-carousel-info").owlCarousel({
					loop:true,
					nav:true,
					items:1,
					navigation: true,
					smartSpeed:250,
					autoplay:true,
					autoplayHoverPause:true
				});
				$(".owl-carousel").owlCarousel({
					loop:true,
					nav:true,
					items:1,
					navigation: true,
					smartSpeed:450,
					autoplay:true,
					autoplayTimeout:8000
				});
				$("#input_search" ).autocomplete({
					source: function( request, response ) {
						// Fetch data
						$.ajax({
							url:"/api/cari-berita",
							type: 'get',
							dataType: "json",
							data: {
								judul_berita: request.term
							},
							success: function( data ) {
							response( data );
							}
						});
					},
					focus: function( event, ui ) {
						event.preventDefault();
						$('#input_search').val(ui.item.label); // display the selected text
					},
					select: function (event, ui) {
						$( "#searchbar" ).submit();
						return false;
					}
				});
				$('#input_search').keypress(function (e) {
					if (e.which == 13) {
						$('#searchbar').submit();
						return false;    //<---- Add this line
					}
				});

				
			});
			
			$('.content-wrapper').click(function(){
					if($('body').hasClass('menu-open')){
						console.log('tutup');
						$body.removeClass('menu-open');
						$('body').removeClass("lock-scroll");
					}
			})
			const $body = $('body');
			const $menuOpen = $('.menu-toggle');
			const $menuClose = $('.cancel-btn');
			const $btnSearch = $('.search-toggle');
			$(function () {
			

			    $menuOpen.click(function() {
			        $body.addClass('menu-open');
					$('body').addClass("lock-scroll");   
			    });
			    $menuClose.click(function() {
			        $body.removeClass('menu-open');
					$('body').removeClass("lock-scroll");
			    });
				
				$btnSearch.click(function() {
					$body.toggleClass('search-open');
				});
				
				
			});
			
			
				
		</script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<!-- Load Facebook SDK for JavaScript -->
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		@yield('script')

		<!-- Modal -->
		<div class="modal fade" id="menus-share" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="background-color: unset;border: unset;">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				...
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
			</div>
		</div>
	</body>
	
</html>