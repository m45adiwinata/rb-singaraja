	
@extends('layouts.main')

<!-- @section('title' , 'Detail Jabatan') -->

@section('content')
	<!-- Set up your HTML -->
			@if(isset($grafis))
			<div class="td-trending-now-wrapper">
				<div class="td-trending-now-title">Info Grafis</div>
				<div class="owl-carousel owl-carousel-info trending-news-slideVertical">
					@foreach($grafis as $data)
						<div class="td-trending-now-display-area"><a href="/berita/{{$data->id}}/detailberita">{{$data->judul}}</a></div>
					@endforeach
				</div>
			</div>
			@endif
			<div class="kgUHa-D row">
					<div class="gBfXnH col-lg-8 col-md-8 col-sm-12 col-xs-12">
					@if (!empty($beritas))
						<div class="owl-carousel slideshow-listHorizontal">
							@foreach ($beritas as $berita)
							<div>
									<div class="media media--text-overlay block-link">
										<div class="media__image">
											<a  href="/berita/{{$berita['id']}}/detailberita" class="media__link">
												<span class="ratiobox ratiobox--16-9 lqd">
													<img src="{{asset($berita['path'])}}" alt="{{asset($berita['judul'])}}" title="{{asset($berita['judul'])}}"> 
												</span>
											</a>
										</div>
										<div class="media__text">
											<h2 class="media__title">
												<a href="/berita/{{$berita['id']}}/detailberita" class="media__link">{{$berita['judul']}}</a>
											</h2>
											<div class="media__date mgt-4">{{$berita['penerbit']}} | <span d-time="1621185021" title="{{date('l, d F Y H:m a', strtotime($berita['created_at']))}}">{{date('l, d F Y H:m a', strtotime($berita['created_at']))}}</span></div>
										</div>
									</div>
									<div class="headline-terkait">
										<div class="headline-terkait__title">Berita Terkait</div>
										<div class="list-content list-content--column grid-row">
											@if($berita['terkaits'])
												@foreach($berita['terkaits'] as $data)
												<article class="list-content__item column">
													<h3 class="list-content__title">
														<a  href="/berita/{{$data->id_relasi_berita}}/detailberita">{{$data->judul}}</a>
													</h3>
												</article>
												@endforeach
											@else
												<article class="list-content__item column">
														<h3 class="list-content__title">
															<a href="#">-</a>
														</h3>
													</article>
																	<article class="list-content__item column">
														<h3 class="list-content__title">
														<a href="#">-<br></a>
														</h3>
													</article>
											@endif
										</div>
									</div>
							</div>
							@endforeach
						</div>
					</div>
					@else
						<div class="owl-carousel slideshow-listHorizontal">
								@for ($i = 0; $i < 2; $i++)
								<div>
										<div class="media media--text-overlay block-link">
										</div>
										<div class="headline-terkait">
										</div>
								</div>
								@endfor
							</div>
						</div>
					@endif
					<div class="dkOPyg col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="Viewweb__StyledView-p5eu6e-0 jqDfEK">
							<div class="Viewweb__StyledView-p5eu6e-0 gkyRKb Headingweb__Wrapper-sc-1k439k1-0 VvtAP">
								<a class="LabelLinkweb-njxxwb-0 cJvuwm" href="/berita"><span class="Textweb__StyledText-sc-1fa9e8r-0 jLsJGy">Popular</span></a>
								<a class="LabelLinkweb-njxxwb-0 cJvuwm" href="/berita">
									<button data-qa-id="ft-loadmore" class="Buttonweb__ButtonWrapper-sc-11sswl3-0 exjjVR">
										<div class="Viewweb__StyledView-p5eu6e-0 drBeOv">
											<div class="Viewweb__StyledView-p5eu6e-0 htSlBT">
												<img name="chevron-next-black icon" data-qa-id="chevron-next-black" src="{{asset('img/menu-vertical.svg')}}" alt="chevron-next-black" class="Iconweb__StyledIcon-qahpco-0 cKFCVl">
											</div>
											<div class="Viewweb__StyledView-p5eu6e-0 jJCycy">
												<span class="Textweb__StyledText-sc-1fa9e8r-0 ekPWUQ">Lihat lainnya</span>
											</div>
										</div>
									</button>
								</a>
							</div>
							<div width="100%" height="1px" class="Viewweb__StyledView-p5eu6e-0 gRorUC"></div>
						</div>
						<div height="553px" class="Viewweb__StyledView-p5eu6e-0 ejgikD">
							@if(!empty($populars))
							<div class="Listweb__Scroll-ra4f1y-0 blRxOX">
								@foreach ($populars as $popular)
								<div class="Viewweb__StyledView-p5eu6e-0 kLvCSr"> <!-- start card -->
									<div style="width: 100%;">
										<div style="width: 100%;">
											<div data-qa-id="trending-story-item" class="Viewweb__StyledView-p5eu6e-0 bSbCiV">
												<div class="Viewweb__StyledView-p5eu6e-0 kcaV">
													<a class="LabelLinkweb-njxxwb-0 cJvuwm" href="/berita/{{$popular->id}}/detailberita">
														<span class="Textweb__StyledText-sc-1fa9e8r-0 jeegbA CardContentweb__CustomText-sc-1wr516g-0 hpLohH" data-qa-id="title">{{$popular->judul}}</span>
														<div class="Viewweb__StyledView-p5eu6e-0 dZTDGp">
															<div class="Viewweb__StyledView-p5eu6e-0 jqDfEK">
																<div class="Viewweb__StyledView-p5eu6e-0 bomQmO">
																	<div class="Viewweb__StyledView-p5eu6e-0 enSBkx">
																		<span class="LabelLinkweb-njxxwb-0 bHjxWK">
																			<span class="LabelLinkweb-njxxwb-0 bHjxWK">
																				<div data-qa-id="avatar" class="Avatarweb__AvatarWrapper-ftedd9-0 iDBAqk">
																					<div color="#FFFFFF" class="Avatarweb__AvatarImageContainer-ftedd9-1 ffynyp">
																						<img src="{{asset('img/sgrjlogo.png')}}" style="width: 101%;height: 101%;">
																					</div>
																				</div>
																			</span>
																		</span>
																	</div>
																	<div class="Viewweb__StyledView-p5eu6e-0 ixPpS">
																		<div class="Viewweb__StyledView-p5eu6e-0 bomQmO">
																			<div class="Viewweb__StyledView-p5eu6e-0 bomQmO">
																				<span class="LabelLinkweb-njxxwb-0 bHjxWK">
																					<div color="gray70" class="TextBoxweb__StyledTextBox-sc-1wzqkk1-0 jwxGoF">
																						<span class="Textweb__StyledText-sc-1fa9e8r-0 iqORTp CardContentweb__NameText-sc-1wr516g-1 cnIZYX" data-qa-id="author-name" style="word-break: break-word; overflow-wrap: break-word;">{{$popular->penerbit()->first()->nama}}</span>
																					</div>
																				</span>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</a>
													
													<div class="Viewweb__StyledView-p5eu6e-0 jmNTGy">
														<div class="Viewweb__StyledView-p5eu6e-0 hbSMcY">
															<a data-qa-id="comment-button-wrapper" class="LabelLinkweb-njxxwb-0 cJvuwm NewsCardFooterweb__ActionLink-sc-1140nk0-2 iVVnGn" href="/berita/{{$popular->id}}/detailberita">
																<img name="comment icon" data-qa-id="comment" src="{{asset('img/binoculars.svg')}}" alt="comment" class="Iconweb__StyledIcon-qahpco-0 bChDNn">
																<span class="Textweb__StyledText-sc-1fa9e8r-0 exBPjh">{{$popular->counter}}</span>
															</a>
															<span class="Textweb__StyledText-sc-1fa9e8r-0 exBPjh"> {{date('l, d F Y H:m a', strtotime($popular->created_at))}}</span>
														</div>
														<!-- <button aria-label="more-button" data-qa-id="more-button-wrapper" data-toggle="modal" data-target="#menus-share" class="NewsCardFooterweb__ActionButton-sc-1140nk0-1 dqpESP">
															<img name="more-vertical icon" data-qa-id="more-vertical" src="{{asset('img/menu-vertical.svg')}}" alt="more-vertical" class="Iconweb__StyledIcon-qahpco-0 bChDNn NewsCardFooterweb__ActionIcon-sc-1140nk0-0 cDnjUS">
														</button> -->
													</div>
												</div>
												<div class="Viewweb__StyledView-p5eu6e-0 jxsxmf">
													<a class="LabelLinkweb-njxxwb-0 cJvuwm" href="/berita/{{$popular->id}}/detailberita">
													<div data-qa-id="image" class="Thumbnailweb__ThumbnailContainer-sc-15nqo19-2 jxacmi">
														<div>
															<div height="100%" width="80px" alt="photo_2021-05-16_14-29-32.jpg" class="Imageweb__ImageWrapper-sc-1kvvof-0 eZCymS" style="background: url({{asset($popular->path)}}) center center / cover repeat;"></div>
															<noscript></noscript>
														</div>
													</div>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div> <!-- end -->
								<div class="Viewweb__StyledView-p5eu6e-0 gdMcah"><div width="100%" height="1px" class="Viewweb__StyledView-p5eu6e-0 gRorUC"></div></div>
								@endforeach
								
							</div>
							@endif
						</div>
						
				</div>
			</div>
			
			<!-- @include('common.pelayanan') -->
			@include('common.birokrasi')

@endsection