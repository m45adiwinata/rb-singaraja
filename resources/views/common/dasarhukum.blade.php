
@extends('layouts.main')

<!-- @section('title' , 'Detail Jabatan') -->

@section('content')
<div>
    @if(!empty($dasarhukum))
        <div class="flex-center-section" style="width:100%">
            <div class="padd-section" style="padding-left: 20px;padding-right: 20px;padding-top: 20px;">
                    <div class="Viewweb__StyledView-p5eu6e-0 bomQmO">
                        <div class="Viewweb__StyledView-p5eu6e-0 ixPpS">
                            <div class="Viewweb__StyledView-p5eu6e-0 bomQmO">
                                <div class="Viewweb__StyledView-p5eu6e-0 bomQmO">
                                    <div class="LabelLinkweb-njxxwb-0 cJvuwm" href="/kumparannews">
                                        <div class="TextBoxweb__StyledTextBox-sc-1wzqkk1-0 eGTKsU" style="background-color: #5B52F8;color: white;padding: 2px 5px;width: fit-content;">
                                            <span id="track_author_name" class="Textweb__StyledText-sc-1fa9e8r-0 gAwWTa" data-qa-id="author-name" style="padding-right:4px;font-size: 15px;">Dasar Hukum</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="story-author">
                        <div class="label-title-news">
                            <h1 style="margin: 0px;" class="title-news bold" data-qa-id="story-title">{{$dasarhukum->nama}}</h1>
                        </div>
                    </div>
                    <div class="text-author">
                            {!! $dasarhukum->isi !!}
                    </div>
                    <div style="min-width: 450px;">
                        @if(!empty($dasarhukum->path))
                            <embed src="{{asset($dasarhukum->path)}}#toolbar=0&navpanes=0&scrollbar=0" width="100%" frameborder="0" height="900"></embed>
                        @endif
                    </div>
                   
        </div>
    @else
        <div style="padding: 15rem 0rem;text-align: center;color: #c9c9c9;font-family: 'Heebo';">
            <h5>-</h5>
        </div>
    @endif
</div>


@endsection