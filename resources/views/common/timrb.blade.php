
@extends('layouts.main')

<!-- @section('title' , 'Detail Jabatan') -->

@section('content')
<div class="container laporan-pad">
    @if(!empty($tim_rb))
        <div class="row">
            <div>
                <div class="tab-content" id="nav-tabContent">
                    <p style="font-weight: 100;">{!! $tim_rb->description !!}</p>
                    <embed src="{{asset($tim_rb->path)}}#toolbar=0&navpanes=0&scrollbar=0" width="100%" frameborder="0" height="700"></embed>
                </div>
            </div>
        </div>
    @else
    <div style="padding: 15rem 0rem;text-align: center;color: #c9c9c9;font-family: 'Heebo';">
        <h5> Tidak ada laporan</h5>
    </div>
    @endif
</div>

@endsection