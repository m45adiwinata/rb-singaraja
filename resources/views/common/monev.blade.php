
@extends('layouts.main')

<!-- @section('title' , 'Detail Jabatan') -->

@section('content')
<div class="container laporan-pad">
    <h1>Monev RB</h1>
    @if(!empty($monev))
        <div class="row">
            <div>
                <div class="tab-content" id="nav-tabContent">
                    <p style="font-weight: 100;">{!! $monev->monev !!}</p>
                </div>
            </div>
        </div>
    @else
    <div style="padding: 15rem 0rem;text-align: center;color: #c9c9c9;font-family: 'Heebo';">
        <h5> Tidak ada laporan</h5>
    </div>
    @endif
</div>

@endsection