
@extends('layouts.main')

<!-- @section('title' , 'Detail Jabatan') -->

@section('content')
<div>
    @if(!empty($judul))
        <div class="flex-center-section">
            <div class="padd-section" style="padding-left: 20px;padding-right: 20px;padding-top: 20px;max-width: 624px;">
                    <div class="Viewweb__StyledView-p5eu6e-0 bomQmO">
                        <div class="Viewweb__StyledView-p5eu6e-0 ixPpS">
                            <div class="Viewweb__StyledView-p5eu6e-0 bomQmO">
                                <div class="Viewweb__StyledView-p5eu6e-0 bomQmO">
                                    <div class="LabelLinkweb-njxxwb-0 cJvuwm" href="/kumparannews">
                                        <div class="TextBoxweb__StyledTextBox-sc-1wzqkk1-0 eGTKsU" style="background-color: #5B52F8;color: white;padding: 2px 5px;width: fit-content;">
                                            <span id="track_author_name" class="Textweb__StyledText-sc-1fa9e8r-0 gAwWTa" data-qa-id="author-name" style="padding-right:4px;font-size: 15px;">Latar Belakang</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="story-author">
                        <div class="label-title-news">
                            <h1 style="margin: 0px;" class="title-news bold" data-qa-id="story-title">{{$judul}}</h1>
                        </div>
                    </div>
                    <div class="text-author">
                            {!! $konten !!}
                    </div>
        </div>
    @endif
</div>


@endsection