
@extends('layouts.main')

<!-- @section('title' , 'Detail Jabatan') -->

@section('content')
<div class="container laporan-pad">
    <h1>Quick Wins</h1>
    @if(!empty($quickwins))
        <div class="row">
            <div>
                <div class="tab-content" id="nav-tabContent">
                    <embed src="{{asset($quickwins->path)}}#toolbar=0&navpanes=0&scrollbar=0" width="100%" frameborder="0" height="700"></embed>
                </div>
            </div>
        </div>
    @else
    <div style="padding: 15rem 0rem;text-align: center;color: #c9c9c9;font-family: 'Heebo';">
        <h5> Tidak ada laporan</h5>
    </div>
    @endif
</div>

@endsection