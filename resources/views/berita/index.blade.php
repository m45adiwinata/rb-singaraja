@extends('layouts.main')

@section('content')
    <div class="padd-section">
        <div class="Viewweb__StyledView-p5eu6e-0 byMVoR">
            <h1 class="Textweb__StyledText-sc-1fa9e8r-0 fdgbpG" data-qa-id="channel-title">Berita</h1>
        </div>
        <div class="Viewweb__StyledView-p5eu6e-0 kknNlO">
            <div class="Viewweb__StyledView-p5eu6e-0 fwdHtx">
                <div width="100%" class="Viewweb__StyledView-p5eu6e-0 lgRuzR">
                    <div class="Viewweb__StyledView-p5eu6e-0 jqDfEK">
                        <div class="Viewweb__StyledView-p5eu6e-0 gkyRKb Headingweb__Wrapper-sc-1k439k1-0 VvtAP">
                            <span class="Textweb__StyledText-sc-1fa9e8r-0 jLsJGy" data-qa-id="channel-feed-header">Feed</span>
                           
                            <div class="row-select-berita row">
                                <div class="col-xs-12 col-sm-5 card h-100" style="border:0px;"><p style="margin:0;text-align:end">Rekomendasi:</p></div>
                                <div class="col-xs-12 col-sm-7">
                                        <select class="select-fill form-control" id="selectbox">
                                            @if(isset($_GET['fill']))
                                                @if($_GET['fill'] == 'bagian-organisasi')
                                                    <option value="bagian-organisasi" selected>Bagian Organisasi Buleleng</option>
                                                    <option value="all">Semua</option>
                                                    <option value="kominfo-buleleng" >Kominfo Buleleng</option>
                                                @elseif($_GET['fill'] == 'kominfo-buleleng')
                                                        <option value="bagian-organisasi" >Bagian Organisasi Buleleng</option>
                                                        <option value="all">Semua</option>
                                                        <option value="kominfo-buleleng" selected>Kominfo Buleleng</option>
                                                @endif
                                            @else
                                                <option value="all" selected>Semua</option>
                                                <option value="bagian-organisasi" >Bagian Organisasi Buleleng</option>
                                                <option value="kominfo-buleleng" >Kominfo Buleleng</option>
                                            @endif
                                        </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="Viewweb__StyledView-p5eu6e-0 gdMcah"></div>
        </div>
        <div class="container-news" id="selected-filter-news-recommandation">
            @foreach($beritas as $key => $berita)
            <a class="nohover" href="/berita/{{$berita->id}}/detailberita">
                 <div class="card-news-fit">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 xol-md-10 col-lg-10">
                                <p class="title-news" style="margin:0px">{{$berita->judul}}</p>
                                <div class="Viewweb__StyledView-p5eu6e-0 dZTDGp">
                                    <div class="Viewweb__StyledView-p5eu6e-0 jqDfEK">
                                        <div class="Viewweb__StyledView-p5eu6e-0 bomQmO">
                                            <div class="Viewweb__StyledView-p5eu6e-0 enSBkx">
                                                <span class="LabelLinkweb-njxxwb-0 bHjxWK">
                                                    <span class="LabelLinkweb-njxxwb-0 bHjxWK">
                                                        <div data-qa-id="avatar" class="Avatarweb__AvatarWrapper-ftedd9-0 iDBAqk">
                                                            <div color="#FFFFFF" class="Avatarweb__AvatarImageContainer-ftedd9-1 ffynyp">
                                                                <img src="{{asset('img/sgrjlogo.png')}}" style="width: 101%; height: 101%;">
                                                            </div>
                                                        </div>
                                                    </span>
                                                </span>
                                            </div>
                                            <div class="Viewweb__StyledView-p5eu6e-0 ixPpS">
                                                <div class="Viewweb__StyledView-p5eu6e-0 bomQmO">
                                                    <div class="Viewweb__StyledView-p5eu6e-0 bomQmO">
                                                        <span class="LabelLinkweb-njxxwb-0 bHjxWK">
                                                            <div color="gray70" class="TextBoxweb__StyledTextBox-sc-1wzqkk1-0 jwxGoF">
                                                                <span class="Textweb__StyledText-sc-1fa9e8r-0 iqORTp CardContentweb__NameText-sc-1wr516g-1 cnIZYX" data-qa-id="author-name" style="word-break: break-word; overflow-wrap: break-word;">{{$berita->penerbit()->first()->nama}}</span>
                                                            </div>
                                                        </span>
                                                    </div>
                                                    <div class="Viewweb__StyledView-p5eu6e-0 jvVPIx">
                                                        <img name="verified-round icon" data-qa-id="verified-round" src="{{asset('img/verified.svg')}}" alt="verified-round" class="Iconweb__StyledIcon-qahpco-0 crBhTt">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="Viewweb__StyledView-p5eu6e-0 jmNTGy">
                                  <div class="Viewweb__StyledView-p5eu6e-0 hbSMcY">
                                        <div class="LabelLinkweb-njxxwb-0 cJvuwm NewsCardFooterweb__ActionLink-sc-1140nk0-2 iVVnGn">
                                            <img src="{{asset('img/binoculars.svg')}}" alt="comment" class="Iconweb__StyledIcon-qahpco-0 bChDNn"/>
                                            <span class="Textweb__StyledText-sc-1fa9e8r-0 exBPjh">{{$berita->counter}}</span>
                                        </div>
                                        <div style="display: flex;align-items: center;justify-content: center;">
                                            <span class="Textweb__StyledText-sc-1fa9e8r-0 exBPjh">{{date('l, d F Y H:m a', strtotime($berita->created_at))}}</span>
                                        </div>
                                    </div>
                                  
                                    <button aria-label="more-button" data-qa-id="more-button-wrapper" class="NewsCardFooterweb__ActionButton-sc-1140nk0-1 dqpESP">
                                        <!-- <img name="more-vertical icon" data-qa-id="more-vertical" src="{{asset('img/menu-vertical.svg')}}" alt="more-vertical" class="Iconweb__StyledIcon-qahpco-0 bChDNn NewsCardFooterweb__ActionIcon-sc-1140nk0-0 cDnjUS"> -->
                                    </button>
                                </div>

                            </div>
                            <div class="col-xs-12 col-sm-6 xol-md-2 col-lg-2">
                                <img src="{{asset($berita->path)}}" alt="{{$berita->judul}}" style="border-radius: 15px;"/>    
                            </div>
                        </div>
                 </div>
                 <div class="Viewweb__StyledView-p5eu6e-0 YFaxK"><div width="100%" height="1px" direction="horizontal" class="Viewweb__StyledView-p5eu6e-0 dPPBAR"></div></div>
            </a>
            @endforeach
            {{ $beritas->links() }}
        </div>

    </div>
    <script>
            // document.addEventListener("DOMContentLoaded", function() {
			// 	var demo1 = new BVSelect({
			// 		selector: "#selectbox",
			// 		searchbox: false,
			// 		offset: true
			// 	});
			// });
    </script>
@endsection
@section('script')
    <script>
        $('.select-fill').change(function(){ 
                var getReq = {!! json_encode($_GET) !!}
                var fill = $(this).val();
                if(fill == 'bagian-organisasi') {
                    window.location.href = '/berita?fill=bagian-organisasi';
                }
                else if(fill == 'kominfo-buleleng') {
                    window.location.href = '/berita?fill=kominfo-buleleng';
                }else{
                    window.location.href = '/berita';
                }
        });
    </script>
@endsection
