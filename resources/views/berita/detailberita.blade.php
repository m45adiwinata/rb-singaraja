@extends('layouts.main')

@section('content')
    <div class="flex-center-section">
        <div class="padd-section" style="padding-left: 20px;padding-right: 20px;padding-top: 20px;max-width: 624px;">
                <div class="Viewweb__StyledView-p5eu6e-0 bomQmO">
                    <div class="Viewweb__StyledView-p5eu6e-0 ffjwvw" style="padding-right: 12px;">
                        <a class="LabelLinkweb-njxxwb-0 cJvuwm" href="#">
                        <span class="LabelLinkweb-njxxwb-0 bHjxWK">
                            <div data-qa-id="author-profile-picture" class="Avatarweb__AvatarWrapper-ftedd9-0 ghUZvP">
                                <div color="#FFFFFF" class="Avatarweb__AvatarImageContainer-ftedd9-1 eFjqMP">
                                    <img src="{{asset('img/sgrjlogo.png')}}" style="width:101%;height:101%">
                                </div>
                            </div>
                        </span>
                        </a>
                    </div>
                    <div class="Viewweb__StyledView-p5eu6e-0 ixPpS">
                        <div class="Viewweb__StyledView-p5eu6e-0 bomQmO">
                            <div class="Viewweb__StyledView-p5eu6e-0 bomQmO">
                                <div class="LabelLinkweb-njxxwb-0 cJvuwm" href="#">
                                    <div class="TextBoxweb__StyledTextBox-sc-1wzqkk1-0 eGTKsU">
                                        <span id="track_author_name" class="Textweb__StyledText-sc-1fa9e8r-0 gAwWTa" data-qa-id="author-name" style="padding-right:4px;font-size: 18px;">{{$berita->penerbit()->first()->nama}}</span>
                                    </div>
                                </div>
                        </div>
                        <div class="Viewweb__StyledView-p5eu6e-0 AParz"><img src="{{asset('img/verifieds.svg')}}" alt="verified-green" class="Iconweb__StyledIcon-qahpco-0 gGURTp"></div>
                        </div>
                    </div>
                </div>
                <div class="story-author">
                    <div class="label-datenews">
                        <div class="news"><h5 class="bold" style="color: rgb(31, 141, 214);margin: 0;">BERITA</h5></div>
                        <div class="date"><h5 style="margin: 0;">{{date('l, d F Y H:m a', strtotime($berita->created_at))}}</h5></div>
                        <div class="LabelLinkweb-njxxwb-0 cJvuwm NewsCardFooterweb__ActionLink-sc-1140nk0-2 iVVnGn" style="margin-left:10px"> 
                            <img src="{{asset('img/binoculars.svg')}}" alt="comment" class="Iconweb__StyledIcon-qahpco-0 bChA"/>
                             <span class="Textweb__StyledText-sc-1fa9e8r-0 exBPjh" style="font-size:15px">{{$berita->counter}} dilihat</span>
                        </div>
                    </div>
                    <div class="label-title-news">
                        <h1 style="margin: 0px;" class="title-news bold" data-qa-id="story-title">{{$berita->judul}}</h1>
                    </div>
                </div>
                <div class="story-image">
                        <img src="{{asset($berita->path)}}" width="100%" alt="" style="border-radius:15px" class="mb-3">
                </div>
                <div class="text-author">
                    {!! $berita->deskripsi !!}
                </div>
                <!-- AddToAny BEGIN -->
                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                    <!-- <a class="a2a_dd" href="https://www.addtoany.com/share"></a> -->
                    <a class="a2a_button_facebook"></a>
                    <a class="a2a_button_twitter"></a>
                    <a class="a2a_button_email"></a>
                </div>
                <script async src="https://static.addtoany.com/menu/page.js"></script>
                <!-- AddToAny END -->
    </div>
@endsection