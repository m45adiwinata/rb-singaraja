@extends('layouts.main')

@section('content')
<div class="container laporan-pad">
    @if(!empty($pdfs) && count($pdfs) != 0)
        <div class="row">
            <div class="col-xs-12 xol-sm-3 col-md-3 col-lg-3" style="margin-bottom: 1rem;">
                <div class="list-group" id="list-tab" role="tablist">
                    @foreach($pdfs as $pdf)
                        @if($loop->first)
                        <a  class="list-group-item list-group-item-action active" id="list-{{$pdf->id}}-list" data-toggle="list" href="#list-{{$pdf->id}}" role="tab" aria-controls="{{$pdf->id}}">{{$pdf->tagline}}</a>
                        @else
                        <a  class="list-group-item list-group-item-action" id="list-{{$pdf->id}}-list" data-toggle="list" href="#list-{{$pdf->id}}" role="tab" aria-controls="{{$pdf->id}}">{{$pdf->tagline}}</a>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="col-xs-12 xol-sm-9 col-md-9 col-lg-9" id="sideright-master-pdf">
                <div class="tab-content" id="nav-tabContent">
                    <!-- bodylist -->
                    @foreach($pdfs as $pdf)
                        @if($loop->first)
                            <div class="tab-pane fade show active" id="list-{{$pdf->id}}" role="tabpanel" aria-labelledby="list-{{$pdf->id}}-list">
                                <h4 style="font-weight: 400;">{{$pdf->title_file}}</h4>
                                <p style="font-weight: 100;">{{$pdf->description}}</p>
                                <embed type="application/pdf" src="{{asset($pdf->path)}}#toolbar=0&navpanes=0&scrollbar=0" width="100%" frameborder="0" height="700"></embed>
                            </div>
                        @else
                            <div class="tab-pane fade" id="list-{{$pdf->id}}" role="tabpanel" aria-labelledby="list-{{$pdf->id}}-list">
                                <h4 style="font-weight: 400;">{{$pdf->title_file}}</h4>
                                <p style="font-weight: 100;">{{$pdf->description}}</p>
                                <embed src="{{asset($pdf->path)}}#toolbar=0&navpanes=0&scrollbar=0" width="100%" frameborder="0" height="700"></embed>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    @else
    <div style="padding: 15rem 0rem;text-align: center;color: #c9c9c9;font-family: 'Heebo';">
        <h5> Tidak ada laporan</h5>
    </div>
    @endif
</div>

@endsection