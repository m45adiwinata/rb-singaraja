  <!-- Site footer -->
  <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <h6>About</h6>
            <p class="text-justify">Bagian Organisasi <br>Pusat Pemerintahan Kabupaten Buleleng</br> Jl. Pahlawan, Paket Agung, Kec. Buleleng <br>Kabupaten Buleleng, Bali<br>(0361) 21985</p>
          </div>

          <!-- <div class="col-xs-6 col-md-3">
            <h6>AREA PERUBAHAN</h6>
            <ul class="footer-links">
							  <li><a href="/birokrasi/1" >Area Manajemen Perubahan</a></li>
									<li><a href="/birokrasi/2">Area Peraturan Perundang-Undangan</a></li>
									<li><a href="/birokrasi/3">Area Kelembagaan</a></li>
									<li><a href="/birokrasi/4">Area Tata Laksana</a></li>
									<li><a href="/birokrasi/5">Area SDM Aparatur</a></li>
									<li><a href="/birokrasi/6">Area Pengawasan</a></li>
									<li><a href="/birokrasi/7">Area Akuntabilitas</a></li>
									<li><a href="/birokrasi/8">Area Pelayanan Publik</a></li>
            </ul>
          </div> -->

          <div class="col-xs-6 col-md-3">
            <h6>DASAR HUKUM</h6>
            <ul class="footer-links">
              @foreach($dasarhukums as $data)
                <li><a href="/profil/dasar-hukum/{{$data->id}}">{{$data->nama}}</a></li>
              @endforeach
            </ul>
          </div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-12">
            <p class="copyright-text">Copyright &copy; {{date("Y")}} Bagian Organisasi</p>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12">
            <ul class="social-icons">
              <li><a class="facebook" href="https://m.facebook.com/story.php?story_fbid=197451048941344&id=100060293501213&sfnsn=wiwspwa"><i class="fa fa-facebook"></i></a></li>
              <li><a class="youtube" href="https://youtu.be/3XvqNGgvDNk"><i class="fa fa-youtube"></i></a></li>
              <li><a class="instagram" href="https://instagram.com/pemkab_buleleng?utm_medium=copy_link"><i class="fa fa-instagram"></i></a></li>
              <!-- <li><a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a></li> -->
              <!-- <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>    -->
            </ul>
          </div>
        </div>
      </div>
</footer>