@extends('admin.layouts.main-admin')
@section('style')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Latar Belakang</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/admin/">Home</a></li>
                <li class="breadcrumb-item active">Profil</li>
                <li class="breadcrumb-item active">Latar Belakang</li>
                </ol>
            </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    @if(Session::has('success'))
        <div class="alert alert-success" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;margin: 1rem 14px;" role="alert">
          <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
        </div>
    @endif
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"></h3>
                            <div class="card-tools">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                </div>
                            </div>
                        </div>
                        <form method="POST" action="{{route('editlatarbelakang')}}" name="add-latarbelakang" enctype="multipart/form-data">
                        @csrf
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="form-group required">
                                    <label for="title" class="control-label">Judul</label>
                                    <input type="text" class="form-control" id="judul" name="judul" placeholder="Input Judul" autocomplete="off" value="{{$latarbelakang->judul}}" required>
                                </div>
                                <div class="form-group required">
                                    <label for="title" class="control-label">Isi</label>
                                    <textarea class="form-control" id="summary-ckeditor" name="konten">{{$latarbelakang->konten}}</textarea>
                                </div>
                            </div>
                            <div class="card-footer" style="text-align:right">
                                    <button id="add-latarbelakang" type="submit" class="btn btn-accen">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
    <!-- /.main content -->
    <!-- Modal Delete -->
    <div class="modal fade" id="modaldelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Hapus Data PDF</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{route('delete-pdf')}}">
                    <div class="modal-body" id="delete-text"></div>
                        @csrf
                        <input type="hidden" id="id-delete" name="id" >
                        <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Iya, Hapus</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.content-wrapper -->
@endsection
@section('script')
<script>
CKEDITOR.replace( 'summary-ckeditor' );
</script>
<script>
        $('#add-latarbelakang').click(function(){
                $("form[name='add-latarbelakang']").validate({
                    rules: {
                        judul:'required',
                        konten:'required',
                    },
                    messages: {
                        judul:"Mohon masukan judul!",
                        konten: "Mohon masukan latarbelakang!",
                        },
                    submitHandler: function(form) {
                        form.submit();
                    }
                });
        });
        setTimeout(function(){ $('.alert').alert('close') }, 3000);
</script>

@endsection