@extends('admin.layouts.main-admin')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Monev RB</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/admin/profil/dasar-hukum">Monev RB</a></li>
                </ol>
            </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    @if(Session::has('success'))
        <div class="alert alert-success" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;margin: 1rem 14px;" role="alert">
          <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
        </div>
    @endif
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <form method="POST" action="{{route('monev-edit')}}" enctype="multipart/form-data">
                            @csrf
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="title" class="control-label">Monev RB</label>
                                        <textarea class="form-control" id="summary-ckeditor1" name="monev">{{$monev ? $monev->monev:''}}</textarea>
                                    </div>
                                </div>
                      
                                <div class="card-footer" style="text-align:right">
                                        <button id="add-latarbelakang" type="submit" class="btn btn-accen">Submit</button>
                                </div>
                            </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
    <!-- /.main content -->
</div>
<!-- /.content-wrapper -->
@endsection
@section('script')
<script>
CKEDITOR.replace( 'summary-ckeditor1' );
</script>
$(function () {
  bsCustomFileInput.init();
});
@endsection