@extends('admin.layouts.main-admin')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Tambah Dasar Hukum</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/admin/profil/dasar-hukum">Dasar Hukum</a></li>
                <li class="breadcrumb-item active">Tambah Dasar Hukum</li>
                </ol>
            </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    @if(Session::has('success'))
        <div class="alert alert-success" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;margin: 1rem 14px;" role="alert">
          <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
        </div>
    @endif
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Tambah Dasar Hukum </h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form method="POST" action="{{route('tambah-dasarhukum')}}" id="adddasarhukum" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                            <!-- /.card-header -->
                                <div class="form-group required">
                                    <label for="title" class="control-label">Judul</label>
                                    <input type="text" class="form-control" id="judul" name="judul" placeholder="judul" autocomplete="off" required>
                                </div>
                                <div class="form-group">
                                    <label for="title">Isi</label>
                                    <textarea class="form-control" id="summary-ckeditor" name="isi"></textarea>
                                </div>
                                <div id="education_fields"> 
                                    <label for="file" class="control-label">PDF</label>
                                    <div class="margver1 row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <input type="file" class="form-control" name="file[]" accept="application/pdf">
                                        </div>
                                    </div>
                                 </div>

                                <div class=" nopadding" style="padding:10px 0">
                                    <div class="input-group-btn" style="width:100%;justify-content:center;display:flex">
                                        <button class="btn btn-warning btn-add-form" type="button"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah PDF </button>
                                    </div>
                                </div>
                                
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer" style="text-align:right">
                                <a href="/admin/profil/dasar-hukum" type="button" class="btn btn-accen2">Kembali</a>
                                <button type="submit" id="add-dasarhukum" class="btn btn-accen">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
    <!-- /.main content -->
</div>
<!-- /.content-wrapper -->
@endsection
@section('script')
<script>
    $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
        $('#add-dasarhukum').click(function(){
                $("form[name='adddasarhukum']").validate({
                    rules: {
                        judul:'required',
                    },
                    messages: {
                        judul: "Mohon masukan judul!",
                    },
                    submitHandler: function(form) {
                        form.submit();
                    }
                });
        });
        
        

        var room = 1;
        $(".btn-add-form").click(function(){ 
            room++;
            var tmp = '';
            var objTo = document.getElementById('education_fields')
            var divtest = document.createElement("div");
            divtest.setAttribute("class", "margver1 row removeclass"+room);
            tmp = tmp + '<div class="col-lg-10 col-md-8 col-sm-8 col-xs-6">'
                    +'<input type="file" class="form-control" name="file[]" accept="application/pdf"></div>'
                    + '<div style="text-align: end; text-decoration: none;" class="col-lg-2 col-md-4 col-sm-4 col-xs-6">  <button onclick="removeComp('+room+');" class="button-remove btn" type="button" style="width: fit-content; font-weight: 700;">Hapus</button> </div>';
            divtest.innerHTML = tmp;
            objTo.appendChild(divtest)
        })

        function removeComp(id){
            document.getElementsByClassName("removeclass"+id)[0].remove();
            $('.alert-danger').show()
        }
        $( document ).ready(function() {
                $('input[type="file"]').change(function(e){
                var fileName = e.target.files[0].name;
                console.log(fileName);
            });
        })

</script>
<script>
CKEDITOR.replace( 'summary-ckeditor' );
</script>
<!-- $(function () {
  bsCustomFileInput.init();
}); -->
@endsection