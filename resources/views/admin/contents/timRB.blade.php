@extends('admin.layouts.main-admin')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Tim RB</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/admin/profil/dasar-hukum">Tim RB</a></li>
                </ol>
            </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    @if(Session::has('success'))
        <div class="alert alert-success" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;margin: 1rem 14px;" role="alert">
          <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
        </div>
    @endif
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <form method="POST" action="{{route('edit-timrb')}}" name="add-latarbelakang" enctype="multipart/form-data">
                            @csrf
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="title" class="control-label">Deskripsi</label>
                                        <textarea class="form-control" id="summary-ckeditor" name="description">{{$timrb->description}}</textarea>
                                    </div>
                                    <div class="form-group ">
                                        <label for="file" class="control-label">File PDF</label><br>
                                        @if(!empty($timrb->path))
                                            @php $pathnew = str_replace('pdf/','',$timrb->path) @endphp
                                            <label style="color:blue">{{$pathnew}}</label>
                                        @endif
                                        <input type="file" class="form-control-file" id="file" name="file" accept="application/pdf" >
                                    </div>
                                </div>
                      
                                <div class="card-footer" style="text-align:right">
                                        <button id="add-latarbelakang" type="submit" class="btn btn-accen">Submit</button>
                                </div>
                            </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
    <!-- /.main content -->
</div>
<!-- /.content-wrapper -->
@endsection
@section('script')
<script>
        // $('#edit-dasarhukum').click(function(){
        //         $("form[name='editdasarhukum']").validate({
        //             rules: {
        //                 isi:'required',
        //             },
        //             messages: {
        //                 isi: "Mohon pilih area jabatan!",
        //                 title: "Mohon masukan judul!",
        //                 tagline: "Mohon masukan tagline max 20 karakter!",
        //                 file: "Mohon masukan file laporan!",
        //                 },
        //             submitHandler: function(form) {
        //                 form.submit();
        //             }
        //         });
        // });
</script>
<script>
CKEDITOR.replace( 'summary-ckeditor' );
</script>
$(function () {
  bsCustomFileInput.init();
});
@endsection