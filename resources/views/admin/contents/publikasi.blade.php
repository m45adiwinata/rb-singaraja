@extends('admin.layouts.main-admin')
@section('style')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Publikasi</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/admin/">Dashboard</a></li>
                <li class="breadcrumb-item active">Publikasi</li>
                </ol>
            </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    @if(Session::has('success'))
        <div class="alert alert-success" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;margin: 1rem 14px;" role="alert">
          <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
        </div>
    @endif
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"></h3>
                            <div class="card-tools">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                </div>
                            </div>
                        </div>
                        <form method="POST" action="{{route('editpublikasi')}}" name="edit-publikasi" enctype="multipart/form-data">
                            @csrf
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="form-group required">
                                    <label for="title" class="control-label">Judul</label>
                                    @if(!empty($publikasi->judul))
                                        <input type="text" class="form-control" id="judul" name="judul" placeholder="Input Judul" autocomplete="off" value="{{$publikasi->judul}}" required>
                                    @else
                                        <input type="text" class="form-control" id="judul" name="judul" placeholder="Input Judul" autocomplete="off" value="" required>
                                    @endif
                                </div>
                                <div class="form-group required">
                                    <label for="title" class="control-label">Isi</label>
                                    <textarea class="form-control" id="summary-ckeditor" name="konten">@if(!empty($publikasi->konten)){{$publikasi->konten}}@endif</textarea>
                                </div>
                                <label for="file" class="control-label">Image</label>
                                <div class=" custom-file form-group">
                                    <label for="file" class="control-label"></label>
                                    <input type="file" class="custom-file-input" id="file" name="file"  accept="image/png, image/jpeg">
                                    @if(!empty($image->path))
                                        @php $pathnew = str_replace('image_berita/','',$image->path) @endphp
                                        <label class="custom-file-label" for="customFile">{{$pathnew}}</label>
                                     @else
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    @endif
                                </div>
                            </div>
                            <div class="card-footer" style="text-align:right">
                                    <button id="add-publikasi" type="submit" class="btn btn-accen">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
    <!-- /.main content -->
</div>
<!-- /.content-wrapper -->
@endsection
@section('script')
<script>
CKEDITOR.replace( 'summary-ckeditor' );
</script>
<script>
        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
        $('#add-publikasi').click(function(){
                $("form[name='edit-publikasi']").validate({
                    rules: {
                        judul:'required',
                        konten:'required',
                    },
                    messages: {
                        judul:"Mohon masukan judul!",
                        konten: "Mohon masukan isi!",
                        },
                    submitHandler: function(form) {
                        form.submit();
                    }
                });
        });
        setTimeout(function(){ $('.alert').alert('close') }, 3000);
</script>

@endsection