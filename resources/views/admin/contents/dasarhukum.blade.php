@extends('admin.layouts.main-admin')
@section('style')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Dasar Hukum</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/admin/">Home</a></li>
                <li class="breadcrumb-item active">Dasar Hukum</li>
                </ol>
            </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    @if(Session::has('success'))
        <div class="alert alert-success" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;margin: 1rem 14px;" role="alert">
          <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
        </div>
    @endif
    <section class="content">
        <div class="container-fluid">
            <div class="card-tools">
                <div class="input-group input-group-sm" style="width: 200px;margin-bottom:1rem">
                    <a class="btn btn-tools btn-sm" href="/admin/profil/dasar-hukum/tambah" style="background-color: #fdfdfd;border: 1px solid #eaeaea;padding: 8px 10px;border-radius: 5px;" ><i class="fas fa-plus-circle"></i> Tambah Dasar Hukum</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md">
                @if(!empty($dasarhukums))
                    @foreach($dasarhukums as $data)
                        <div class="info-box">
                            <div class="info-box-content">{{$data->nama}}</div>
                            <a class="btn" href="/admin/profil/dasar-hukum/{{$data->id}}"><span class="info-box-icon bg-cyan" style="height: 100%;"><i class="fa fa-search"></i></span></a>
                            <a class="btn" onclick="dasarhukum.delete({{$data->id}})"><span class="info-box-icon bg-red" style="height: 100%;"><i class="fa fa-trash"></i></span></a>
                        </div>
                    @endforeach
                @else
                    <div>data tidak ditemukan</div>
                @endif
                </div>
            </div>
        </div>
    </section>
    <!-- /.main content -->
</div>
<!-- /.content-wrapper -->

    <!-- Modal Delete -->
    <div class="modal fade" id="modaldeletedasarhukum" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Hapus Dasar Hukum</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="{{route('delete-dasarhukum')}}" id="delete-dasarhukum" enctype="multipart/form-data">
                    @csrf
                        <div class="modal-body" id="delete-text"></div>
                            <input type="hidden" id="id" name="id" >
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Iya, Hapus</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection
@section('script')
<script>
        var dasarhukum = { 
                'delete': function(id) {
                    $('#delete-dasarhukum #delete-text').html('Anda yakin ingin menghapus Dasar Hukum ini?');
                    $('#delete-dasarhukum #id').val(id);
                    $('#modaldeletedasarhukum').modal('show'); 
                }
        }
    </script>
@endsection