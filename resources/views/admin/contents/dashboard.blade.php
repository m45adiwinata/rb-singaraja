@extends('admin.layouts.main-admin')
@section('style')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
<div class="content-wrapper">
 <!-- Main content -->
 <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row" style="padding-top: 1rem;">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-purple">
              <div class="inner">
                @if(isset($count_berita_all))
                    <h3>{{$count_berita_all}}</h3>
                @else
                    <h3>0</h3>
                @endif
                <p>Berita</p>
              </div>
              <div class="icon">
              <i class="fas fa-newspaper"></i>
              </div>
              <a href="/admin/berita/daftar-berita" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-blue">
              <div class="inner">
                @if(isset($count_laporan_all))
                    <h3>{{$count_laporan_all}}</h3>
                @else
                    <h3>0</h3>
                @endif
                <p>Laporan Area Birokrasi</p>
              </div>
              <div class="icon">
              <i class="fas fa-file"></i>
              </div>
              <a href="/admin/pdf" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-light">
              <div class="inner">
                @if(isset($count_kategori_all))
                    <h3>{{$count_kategori_all}}</h3>
                @else
                    <h3>0</h3>
                @endif
                <p>Kategori Berita</p>
              </div>
              <div class="icon">
              <i class="fas fa-list"></i>
              </div>
              <a href="/admin/master/kategori-berita" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-pink">
              <div class="inner">
                @if(isset($count_users_all))
                    <h3>{{$count_users_all}}</h3>
                @else
                    <h3>0</h3>
                @endif
                <p>Staff</p>
              </div>
              <div class="icon">
              <i class="fas fa-users"></i>
              </div>
              <a href="/admin/master/users" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

        </div>
    </div>
</section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
                <div class="col-md-6">
                @if(isset($beritas))
                <div class="card card-warning table-responsive">
                  <div class="card-header">
                          <h3 class="card-title">Berita terpopuler</h3>
                  </div>
                  <table class="table ">
                      <thead >
                        <tr class="bg-warning">
                          <th scope="col">#</th>
                          <th scope="col">Judul</th>
                          <th scope="col">Viewer</th>
                          <th scope="col"></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($beritas as $key => $data)
                          <tr>
                            <th scope="row">{{$key+1}}</th>
                            <td>{{$data->judul}}</td>
                            <td>{{$data->counter}}</td>
                            <td><a class="btn btn-navbar" href="/admin/berita/{{$data->id}}/edit"><i class="fas fa-search"></i></a></td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>
                   
                  @endif
                </div>
                <div class="col-md-6">
                    <!-- DONUT CHART -->
                    <div class="card card-danger">
                    <div class="card-header">
                        <h3 class="card-title">Laporan Area Perubahan Chart</h3>

                        <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <canvas id="donutChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                    </div>
                    <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>

    //-------------
    //- DONUT CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
         var chart1 = parseInt({{$area1}});
         var chart2 = parseInt({{$area2}});
         var chart3 = parseInt({{$area3}});
         var chart4 = parseInt({{$area4}});
         var chart5 = parseInt({{$area5}});
         var chart6 = parseInt({{$area6}});
         var chart7 = parseInt({{$area7}});
         var chart8 = parseInt({{$area8}});
    var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
    var donutData        = {
      labels: [
          'Area Manajemen Perubahan',
          'Area Peraturan Perundang-Undangan',
          'Area Kelembagaan',
          'Area Tata Laksana',
          'Area SDM Aparatur',
          'Area Pengawasan',
          'Area Akuntabilitas',
          'Area Pelayanan Publik',

      ],
      datasets: [
        {
          

          data: [chart1,chart2,chart3,chart4,chart5,chart6,chart7,chart8],
          backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de', '#20c997', '#e83e8c'],
        }
      ]
    }
    var donutOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var donutChart = new Chart(donutChartCanvas, {
      type: 'doughnut',
      data: donutData,
      options: donutOptions
    })
</script>
@endsection