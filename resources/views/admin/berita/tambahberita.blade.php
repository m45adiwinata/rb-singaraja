@extends('admin.layouts.main-admin')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Tambah Berita</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/admin/berita/daftar-berita">Daftar Berita</a></li>
                <li class="breadcrumb-item active">Tambah Berita</li>
                </ol>
            </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <!-- <h3 class="card-title">File File Birokrasi</h3> -->
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form method="POST" action="{{route('tambah-berita')}}" name="addberita" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="form-group required">
                                    <label for="title" class="control-label">Judul</label>
                                    <input type="text" class="form-control" id="judul" name="judul" placeholder="Input Judul" autocomplete="off" required>
                                </div>
                                <div class="form-group required">
                                    <label for="id_kategori" class="control-label">Kategori Berita</label>
                                    <select class="form-control select2" id="id_kategori" name="id_kategori">
                                        @if (!empty($categories))
                                            @foreach($categories as $key => $category)
                                                @if($loop->first)
                                                    <option value="{{$category->id}}" selected="selected">{{$category->nama}}</option>
                                                @else
                                                    <option value="{{$category->id}}">{{$category->nama}}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group ">
                                    <label for="title" class="control-label">Kutipan</label>
                                    <input type="text" class="form-control" id="kutipan" name="kutipan" placeholder="Input kutipan max 20 karakter" autocomplete="off" maxlength="100" >
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Deskripsi Berita</label>
                                    <textarea class="form-control" id="summary-ckeditor" name="deskripsi" required></textarea>
                                </div>
                                <div class="form-group required">
                                    <label for="id_penerbit" class="control-label">Penerbit</label>
                                    <select class="form-control select2" id="id_penerbit" name="id_penerbit">
                                    @if (!empty($penerbits))
                                        @foreach($penerbits as $key => $penerbit)
                                            @if($loop->first)
                                                <option value="{{$penerbit->id}}" selected="selected">{{$penerbit->nama}}</option>
                                            @else
                                                <option value="{{$penerbit->id}}">{{$penerbit->nama}}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                    </select>
                                </div>
                                <label for="file" class="control-label">Sampul Berita</label>
                                <div class=" custom-file form-group required">
                                    <label for="file" class="control-label"></label>
                                    <input type="file" class="custom-file-input" id="file" name="file"  accept="image/png, image/jpeg" required>
                                     <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>

                                <label for="file" class="control-label">Berita Terkait</label>
                                <select class="selectpicker" multiple data-live-search="true" name="relasi[]" >
                                    <option value="-">-</option>
                                    @if(!empty($beritas))
                                        @foreach($beritas as $berita)
                                            <option value="{{$berita->id}}">{{$berita->judul}}</option>
                                        @endforeach
                                    @endif
                                </select>

                                <label for="file" class="control-label" style="margin-top: 1rem;">Status Berita</label>
                                <div class="form-check">
                                    <input class="form-check-input" value="1" type="radio" name="status" id="status1" checked>
                                    <label class="form-check-label" for="status1">
                                        Publish
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" value="0" type="radio" name="status" id="status2">
                                    <label class="form-check-label" for="status2">
                                        Draft
                                    </label>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer" style="text-align:right">
                                <a type="button" class="btn btn-accen2" href="/admin/berita/daftar-berita">Batal</a>
                                <button id="add-berita" type="submit" class="btn btn-accen">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
    <!-- /.main content -->
</div>
<!-- /.content-wrapper -->

@endsection
@section('script')

<script>
        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
        $('#add-berita').click(function(){
                $("form[name='addberita']").validate({
                    rules: {
                        judul:'required',
                        id_kategori : 'required',
                        deskripsi : 'required',
                        id_penerbit : 'required',
                        file : 'required',
                    },
                    messages: {
                        judul: "Mohon masukan judul berita!",
                        id_kategori: "Mohon pilih kategori berita!",
                        deskripsi: "Mohon masukan deskripsi berita",
                        id_penerbit: "Mohon penerbit",
                        file: "Mohon masukan foto untuk sampul berita",
                        },
                    submitHandler: function(form) {
                        form.submit();
                    }
                });
        });

        $('select').selectpicker();
</script>
<script>
CKEDITOR.replace( 'summary-ckeditor' );
</script>
@endsection