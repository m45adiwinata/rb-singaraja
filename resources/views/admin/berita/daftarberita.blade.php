@extends('admin.layouts.main-admin')
@section('style')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Daftar Berita</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Daftar Berita</li>
                </ol>
            </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    @if(Session::has('success'))
        <div class="alert alert-success" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;margin: 1rem 14px;" role="alert">
          <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
        </div>
    @endif
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md">
                    <div class="card">
                    <div class="card-header" style="display: flex;">
                        <!-- <h3 class="card-title">Daftar Berita</h3> -->
                        <div class="input-group input-group-sm" style="width: 250px;">
                            <input type="text" name="search" id="search" class="form-control float-left" placeholder="Search" value="{{isset($_GET['search']) ? $_GET['search'] : ''}}">
                            <div class="input-group-append">
                            <button class="btn btn-default" id="submit-search">
                                <i class="fas fa-search"></i>
                            </button>
                            </div>
                        </div>
                        <div class="card-tools" style="display: flex;align-items: center;width: 100%;justify-content: flex-end;">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <a class="btn btn-tools btn-sm" href="/admin/berita/tambah-form"><i class="fas fa-plus-circle"></i> Tambah Berita</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body card-news">
                    @if(!empty($beritas))
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                            <th style="width: 10px">#</th>
                            <th>Foto</th>
                            <th>Judul</th>
                            <th>Kategori</th>
                            <th>Status</th>
                            <th>Dilihat</th>
                            <th>Tanggal</th>
                            <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($beritas as $key => $berita)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td><img src="{{asset($berita->path)}}" alt=""  height=150 width=150></img></td>
                                    <td>{{$berita->judul}}</td>
                                    <td>{{$berita->kategori()->first()->nama}}</td>
                                    <td style="width:50px">@if($berita->status == 1)<label class="label-berita">Publish</label>@else<label class="label-draft">Draft</label>@endif</td>
                                    <td style="width:50px">{{$berita->counter}}</td>
                                    <td style="width:150px">{{date('l, d F Y H:m a', strtotime($berita->created_at))}}</td>
                                    <td>
                                         <a class="btn" href="/admin/berita/{{$berita->id}}/edit"><i class="fas fa-edit"></i></a>
                                         <button class="btn" onclick="berita.delete({{$berita->id}})"><i class="far fa-trash-alt"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        
                        </table>
                        @else
                           <div style="text-align:center">Data belum tersedia</div>
                        @endif
                        {{ $beritas->links() }}

                    </div>
                    </div>
                    <!-- /.card -->
                </div>
                <div class="container-fluid">
                    @if(!empty($var_search))
                    <p class="m-0" style="text-align:center">search of - <label style="font-weight:bold">{{$var_search}}</label></p>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!-- /.main content -->
   
       <!-- Modal Delete -->
       <div class="modal fade" id="modaldeleteberita" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Hapus Berita</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{route('delete-berita')}}" id="delete-berita" enctype="multipart/form-data">
                @csrf
                    <div class="modal-body" id="delete-text"></div>
                        <input type="hidden" id="id" name="id" >
                        <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Iya, Hapus</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<!-- /.content-wrapper -->
@endsection

@section('script')

<script>

    $("#submit-search").click(function() {
      if($('#search').val() == "") {
          window.location.href = '/admin/berita/daftar-berita';
      }
      else {
          window.location.href = '/admin/berita/daftar-berita?search='+$('#search').val();
      }
    });
var berita = { 
                'delete': function(id) {
                    $('#delete-berita #delete-text').html('Anda yakin ingin menghapus Berita ini?');
                    $('#delete-berita #id').val(id);
                    $('#modaldeleteberita').modal('show'); 
                }
        }
    </script>
@endsection