<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RB Buleleng | Admin</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <!-- <link rel="stylesheet" href="{{asset('adminasset/plugins/fontawesome-free/css/all.min.css')}}"> -->
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="{{asset('adminasset/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('adminasset/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{{asset('adminasset/plugins/jqvmap/jqvmap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('adminasset/dist/css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('adminasset/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('adminasset/plugins/daterangepicker/daterangepicker.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('adminasset/plugins/summernote/summernote-bs4.min.css')}}">

    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}"  type='image/x-icon'>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />

    <!-- <link href="https://www.foo.com/wp-content/themes/independent-publisher/fonts/fontawesome-free-5-0-6/web-fonts-with-css/fontawesome-all.css" rel="stylesheet"> -->


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.4/css/fontawesome.min.css" integrity="sha384-jLKHWM3JRmfMU0A5x5AkjWkw/EYfGUAGagvnfryNV3F9VqM98XiIH7VBGVoxVSc7" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    @yield('style')
    </head>
    <style>
    .nav-pills .nav-link{
        color: #fff
    }
    .nav-treeview .nav-item .nav-link{
        color:#20c997;
    }
    .nav-treeview .nav-item .nav-link:not(.active):hover{
        color:#3fb18f
    }
    .nav-sidebar .nav-header{
        color:#fff
    }
    .nav-pills .nav-link:not(.active):hover{
        color:#fff
    }
    .nav-pills .nav-link.active, .nav-pills .show>.nav-link{
        background-color: #4bd0a9;
    }
    .main-sidebar{
        background-color: #20c997;
    }
    .nav-sidebar .menu-open>.nav-treeview{
        display: block;
        background-color: #fff!important;
        border-radius: 5px;
        transition:0.3s;
    }
    .custom-items .nav-item1>.nav-link{
        color:#5d5d5d!important;
    }
    label.error {
    color: #ff7878;
    font-weight: normal;
    }
    .form-control.error {
        border: 1px solid #ff6767;
    }
    .brand-text {
        text-transform: uppercase;
        font-size: 15px;
        text-align: center;
        color: #fff;
        font-weight: 700;
    }
    .brand-link .brand-image{
        float: unset;
    }
    .input-group-append{
        background-color: #1db98b;
        border-top-right-radius: 6px;
        border-bottom-right-radius: 6px;
    }
    .btn-sidebar{
        color: white;
    }
    .btn-accen{
        background-color: #1db98b;
        color: white;
    }
    .btn-accen-2{
        background-color: transparent;
        color: #20c997;
    }
    .label-berita{
        background-color: #12b912;
        padding: 2px 5px;
        border-radius: 5px;
        color: white;
        font-weight: 600;
        font-size: 12px;
    }

    .label-draft{
        background-color: #f14146;
        padding: 2px 5px;
        border-radius: 5px;
        color: white;
        font-weight: 600;
        font-size: 12px;
    }

.bootstrap-select{
    width: 100%!important;
}

.card-news nav{
    margin: 1rem 0rem;
    display: flex;
    justify-content: flex-end
}
.card-news nav .page-item .page-link{
    color:#20c997;
}

.card-news nav .page-item.active .page-link{
    background-color: #20c997;
    border-color: #20c997;
    color:#fff;
}

@media(max-width:600px){
    .card{
        width: 100%;
    position: relative;
    overflow-x: scroll;
    }
}
@media (min-width: 992px){
.sidebar-mini.sidebar-collapse .main-sidebar {
    overflow-x:unset;
}
}

.flex-detail-btn{
  display: flex;
  align-items: center;
  justify-content: space-between;
  background-color: #f4f4f4;
  border: 1px solid #d0d0d0;
  padding: 5px;
  border-radius: 5px;
  margin:10px 0;
}

.margver1{
  margin-bottom: 1rem;
}

.button-remove {
    background-color: #c72f2f!important;
    color: white!important;
    border-radius: 10px;
    border: none;
}
.custom-file{
    /* display:flex!important; */
}
    </style>
    <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <!-- <li class="nav-item d-none d-sm-inline-block">
            <a href="index3.html" class="nav-link">Home</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link">Contact</a>
        </li> -->
        </ul>

        <!-- <form class="form-inline ml-3">
        <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
            <button class="btn btn-navbar" type="submit">
                <i class="fas fa-search"></i>
            </button>
            </div>
        </div>
        </form>  -->

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="fas fa-sign-out-alt"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                <a href="/logout" class="dropdown-item">
                    <i class="fas fa-exit"></i> Logout
                </a>
            </div>
        </li>
        <!-- <li class="nav-item">
            <a class="nav-link" data-widget="fullscreen" href="#" role="button">
            <i class="fas fa-expand-arrows-alt"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
            <i class="fas fa-th-large"></i>
            </a>
        </li> -->
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar elevation-4">
        <!-- Brand Logo -->
        <a href="#" class="brand-link" style="text-decoration:none">
        <img src="{{asset('img/sgrjlogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle " >
        <span class="brand-text ">RB-Buleleng</span>
        </a>
        <br>
        <!-- Sidebar -->
        <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <!-- <div class="user-panel mt-3 pb-3 mb-3 d-flex"> -->
            <!-- <div class="image">
            <img src="{{asset('adminasset/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
            <a href="#" class="d-block">Alexander Pierce</a>
            </div> -->
        <!-- </div> -->

        <!-- SidebarSearch Form -->
        <!-- <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
            <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
                <button class="btn btn-sidebar">
                <i class="fas fa-search fa-fw"></i>
                </button>
            </div>
            </div>
        </div> -->

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-header">Menu</li>
            <li class="nav-item">
                @if(!empty($sideaktif) && $sideaktif == 6)
                    <a href="/admin/" class="nav-link active">
                @else
                    <a href="/admin/" class="nav-link">
                @endif
                    <i class="nav-icon fas fa-home"></i>
                    <p>
                        Dashboard
                    </p>
                </a>
            </li>
            <li class="nav-item">
                @if(!empty($sideaktif) && $sideaktif == 1)
                    <a href="#" class="nav-link active">
                        <i class="nav-icon fas fa-table"></i>
                        <p>Profil<i class="fas fa-angle-left right"></i></p>
                    </a>
                @else
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-table"></i>
                        <p>Profil<i class="fas fa-angle-left right"></i></p>
                    </a>
                @endif
                <ul class="nav nav-treeview custom-items">
                    <li class="nav-item">
                        <a href="/admin/profil/latar-belakang" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Latar Belakang</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/profil/dasar-hukum" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Dasar Hukum</p>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- <li class="nav-item">
                @if(!empty($sideaktif) && $sideaktif == 3.1)
                <a href="/admin/roadmap-rb" class="nav-link active"><i class="nav-icon fas fa-home"></i>Roadmap RB</a>
                @else
                <a href="/admin/roadmap-rb" class="nav-link"><i class="nav-icon fas fa-home"></i>Roadmap RB</a>
                @endif
            </li> -->
            <li class="nav-item">
                @if(!empty($sideaktif) && $sideaktif == 3.2)
                <a href="/admin/quickwins" class="nav-link active"><i class="nav-icon fas fa-file"></i>Quick Wins</a>
                @else
                <a href="/admin/quickwins" class="nav-link"><i class="nav-icon fas fa-file"></i>Quick Wins</a>
                @endif
            </li>
            <li class="nav-item">
                @if(!empty($sideaktif) && $sideaktif == 3.3)
                <a href="/admin/monev-rb" class="nav-link active"><i class="nav-icon fas fa-table"></i>Monev RB</a>
                @else
                <a href="/admin/monev-rb" class="nav-link"><i class="nav-icon fas fa-table"></i>Monev RB</a>
                @endif
            </li>
            <li class="nav-header">BIROKRASI</li>
            <!-- <li class="nav-item">
                @if(!empty($sideaktif) && $sideaktif == 2)
                    <a href="/admin/pdf" class="nav-link active">
                @else
                    <a href="/admin/pdf" class="nav-link">
                @endif
                    <i class="nav-icon fas fa-file"></i>
                    <p>
                        8 Area Perubahan
                    </p>
                </a>
            </li> -->
            <li class="nav-item">
                @if(!empty($sideaktif) && $sideaktif == 7)
                    <a href="/admin/tim-rb" class="nav-link active">
                @else
                    <a href="/admin/tim-rb" class="nav-link">
                @endif
                    <i class="nav-icon fas fa-circle"></i>
                    <p>
                        Tim RB
                    </p>
                </a>
            </li>
            <li class="nav-item">
                @if(!empty($sideaktif) && $sideaktif == 8)
                    <a href="/admin/publikasi" class="nav-link active">
                @else
                    <a href="/admin/publikasi" class="nav-link">
                @endif
                    <i class="nav-icon fa fa-camera"></i>
                    <p>
                        Publikasi
                    </p>
                </a>
            </li>
            <li class="nav-header">BERITA</li>
            <li class="nav-item">
                @if(!empty($sideaktif) && $sideaktif == 3)
                    <a href="/admin/berita/daftar-berita" class="nav-link active">
                @else
                    <a href="/admin/berita/daftar-berita" class="nav-link">
                @endif
                    <i class="nav-icon fas fa-file"></i>
                    <p>
                        Berita
                    </p>
                </a>
            </li>
            <li class="nav-header">MASTER</li>
            <li class="nav-item">
                @if(!empty($sideaktif) && $sideaktif == 5)
                    <a href="/admin/master/kategori-berita" class="nav-link active">
                @else
                    <a href="/admin/master/kategori-berita" class="nav-link">
                @endif
                    <i class="nav-icon fas fa-list"></i>
                    <p>
                        Kategori Berita
                    </p>
                </a>
            </li>
            <li class="nav-item">
                @if(!empty($sideaktif) && $sideaktif == 9)
                    <a href="/admin/master/users" class="nav-link active">
                @else
                    <a href="/admin/master/users" class="nav-link">
                @endif
                    <i class="nav-icon fas fa-user"></i>
                    <p>
                        Users
                    </p>
                </a>
            </li>
             <!--<li class="nav-header">MISCELLANEOUS</li>
            <li class="nav-item">
                <a href="iframe.html" class="nav-link">
                <i class="nav-icon fas fa-ellipsis-h"></i>
                <p>Tabbed IFrame Plugin</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="https://adminlte.io/docs/3.1/" class="nav-link">
                <i class="nav-icon fas fa-file"></i>
                <p>Documentation</p>
                </a>
            </li>
            <li class="nav-header">MULTI LEVEL EXAMPLE</li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Level 1</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                <i class="nav-icon fas fa-circle"></i>
                <p>
                    Level 1
                    <i class="right fas fa-angle-left"></i>
                </p>
                </a>
                <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Level 2</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>
                        Level 2
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                        <i class="far fa-dot-circle nav-icon"></i>
                        <p>Level 3</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                        <i class="far fa-dot-circle nav-icon"></i>
                        <p>Level 3</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                        <i class="far fa-dot-circle nav-icon"></i>
                        <p>Level 3</p>
                        </a>
                    </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Level 2</p>
                    </a>
                </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Level 1</p>
                </a>
            </li>
            <li class="nav-header">LABELS</li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                <i class="nav-icon far fa-circle text-danger"></i>
                <p class="text">Important</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                <i class="nav-icon far fa-circle text-warning"></i>
                <p>Warning</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                <i class="nav-icon far fa-circle text-info"></i>
                <p>Informational</p>
                </a>
            </li> -->
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>
    
    @yield('content')

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- jQuery -->
<script src="{{asset('adminasset/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('adminasset/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)

  setTimeout(function(){ $('.alert').alert('close') }, 3000);
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('adminasset/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- bs-custom-file-input -->
<script src="{{asset('adminasset/plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('adminasset/plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('adminasset/plugins/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->
<script src="{{asset('adminasset/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{asset('adminasset/plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('adminasset/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('adminasset/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('adminasset/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('adminasset/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('adminasset/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('adminasset/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('adminasset/dist/js/adminlte.js')}}"></script>

<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

<!-- AdminLTE for demo purposes -->
<!-- <script src="dist/js/demo.js"></script> -->
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="dist/js/pages/dashboard.js"></script> -->
@yield('script')
</body>
</html>