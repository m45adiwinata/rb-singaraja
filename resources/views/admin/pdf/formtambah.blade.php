@extends('admin.layouts.main-admin')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Tambah File Birokrasi</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Tambah File Birokrasi</li>
                </ol>
            </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">File File Birokrasi</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form method="POST" action="{{route('tambah-pdf')}}" name="addpdf" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="form-group required">
                                    <label for="jab-atasan" class="control-label">Area Perubahan</label>
                                    <select class="form-control select2" id="id_area_perubahan" name="id_area_perubahan" style="width: 100%;" required>
                                            <option selected="selected" value="-">-</option>
                                            @foreach($area_perubahans as $key => $area)
                                                <option value="{{$area->id}}">{{$area->area_perubahan}}</option>
                                            @endforeach
                                    </select>
                                </div>
                                <div class="form-group required">
                                    <label for="title" class="control-label">Judul</label>
                                    <input type="text" class="form-control" id="title" name="title" placeholder="Input Judul" autocomplete="off" required>
                                </div>
                                <div class="form-group required">
                                    <label for="title" class="control-label">Tagline</label>
                                    <input type="text" class="form-control" id="tagline" name="tagline" placeholder="Input Tagline max 20 karakter" autocomplete="off" maxlength="20" required>
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Deskripsi</label>
                                    <textarea class="form-control" rows="3" placeholder="Input Deskripsi" name="description"></textarea>
                                </div>
                                <div class="form-group required">
                                    <label for="file" class="control-label">File PDF</label>
                                    <input type="file" class="form-control-file" id="file" name="file" accept="application/pdf" required>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer style="text-align:right"">
                                <button id="add-pdf" type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
    <!-- /.main content -->
</div>
<!-- /.content-wrapper -->

@endsection
@section('script')
<script>
        $('#add-pdf').click(function(){
                $("form[name='addpdf']").validate({
                    rules: {
                        id_area_perubahan:'required',
                        title : 'required',
                        tagline : 'required',
                        file : 'required',
                    },
                    messages: {
                        id_area_perubahan: "Mohon pilih area jabatan!",
                        title: "Mohon masukan judul!",
                        tagline: "Mohon masukan tagline max 20 karakter!",
                        file: "Mohon masukan file laporan!",
                        },
                    submitHandler: function(form) {
                        form.submit();
                    }
                });
        });
</script>

$(function () {
  bsCustomFileInput.init();
});
@endsection