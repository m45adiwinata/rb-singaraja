@extends('admin.layouts.main-admin')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Edit File Birokrasi</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Edit File Birokrasi</li>
                </ol>
            </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Edit File Birokrasi</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form method="POST" action="{{route('edit-pdf', $pdf->id)}}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="form-group required">
                                    <label for="jab-atasan" class="control-label">Area Perubahan</label>
                                    <select class="form-control select2" id="id_area_perubahan" name="id_area_perubahan" style="width: 100%;" required>
                                            @foreach($area_perubahans as $key => $area)
                                                @if($area->id == $pdf->id_area_perubahan)
                                                    <option selected="selected" value="{{$area->id}}">{{$area->area_perubahan}}</option>
                                                @else
                                                    <option value="{{$area->id}}">{{$area->area_perubahan}}</option>
                                                @endif
                                            @endforeach
                                    </select>
                                </div>
                                <div class="form-group required">
                                    <label for="title" class="control-label">Title</label>
                                    <input type="text" class="form-control" id="title" name="title" placeholder="Input Title" autocomplete="off" value="{{$pdf->title_file}}" required>
                                </div>
                                <div class="form-group required">
                                    <label for="title" class="control-label">Tagline</label>
                                    <input type="text" class="form-control" id="tagline" name="tagline" placeholder="Input Tagline max 20 karakter" autocomplete="off" value="{{$pdf->tagline}}" maxlength="20" required>
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Deskripsi</label>
                                    <textarea class="form-control" rows="3" placeholder="Enter ..." name="description">{{$pdf->description}}</textarea>
                                </div>
                                <div class="form-group">
                                    
                                    <label for="file">File PDF</label><br>
                                    @if(!empty($pdf->path))
                                        @php $pathnew = str_replace('pdf/','',$pdf->path) @endphp
                                    <label style="color:blue">{{$pathnew}}</label>
                                    @endif
                                    <input type="file" class="form-control-file" id="file" name="file">
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
    <!-- /.main content -->
</div>
<!-- /.content-wrapper -->
@endsection
@section('script')
<script>
        $('#add-pdf').click(function(){
                $("form[name='addpdf']").validate({
                    rules: {
                        id_area_perubahan:'required',
                        title : 'required',
                        tagline : 'required',
                        file : 'required',
                    },
                    messages: {
                        id_area_perubahan: "Mohon pilih area jabatan!",
                        title: "Mohon masukan judul!",
                        tagline: "Mohon masukan tagline max 20 karakter!",
                        file: "Mohon masukan file laporan!",
                        },
                    submitHandler: function(form) {
                        form.submit();
                    }
                });
        });
</script>

$(function () {
  bsCustomFileInput.init();
});
@endsection