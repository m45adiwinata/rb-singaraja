<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('loginform/fonts/icomoon/style.css')}}">
    <link rel="stylesheet" href="{{asset('loginform/css/owl.carousel.min.css')}}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('loginform/css/bootstrap.min.css')}}">
    <!-- Style -->
    <link rel="stylesheet" href="{{asset('loginform/css/style.css')}}">
    <title>ADMIN RB - Kab Buleleng</title>
  </head>
  <body cz-shortcut-listen="true" class="zcx78swhhh">
  <div class="content">
    <div class="container">
      <div id="header"><h1>ADMIN RB - ONLINE</h1></div>
      <div class="row" id="row-box">
        <div class="hidden-xs col-sm-12 col-md-2 col-lg-2"></div>
        <div class="col-sm-12 col-sm-12 col-md-5 col-lg-5 no-padding">
              <div class="cover-mascot">
                  <img src="{{asset('loginform/images/singmascot.jpg')}}" class="img-cover">
              </div>
              <div class="company-information">
                  <img src="{{asset('loginform/images/sgrjlogo.png')}}">
                  <h4>Bagian Organisasi</h4>
                  <h5>KABUPATEN BULELENG</h5>
                  <p>
                      Pusat Pemerintahan Kabupaten Buleleng<br>
                      Jl. Pahlawan, Paket Agung, Kec. Buleleng<br>
                      Kabupaten Buleleng, Bali<br>
                  </p>
              </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-sm-3 no-padding contents bg-right">
          <div class="title-right-login">
                    <h3 style="margin-top: 10px;font-weight: 300;">LOGIN USER</h3>
          </div>
          <div class="row justify-content-center">
            <div class="col-md-10">
              <div class="login-information-right">
                  <p class="mb-4"> Silahkan masukkan username dan password anda!</p>
              </div>
              @if(Session::has('fail'))
              <div class="alert alert-fail" style="padding: 5px 10px;background-color: #ebc3c3;color: #9c0b0b; border-radius:5px; font-size:12px;" role="alert">
                <i class="fas fa-exclamation-circle"></i> {{ Session::get('fail') }}
              </div>
              @endif
            <form action="{{ route('login') }}" method="post">
                @csrf
              <div class="form-group first">
                <label for="username">Username</label>
                <input type="text" class="form-control" id="username" name="username">
              </div>
              <div class="form-group last mb-4">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password">
              </div>
              <input type="submit" value="Log In" class="btn text-white btn-block btn-primary">
            </form>
            </div>
          </div>
        </div>
        <div class="hidden-xs col-sm-12 col-md-2 col-lg-2"></div>
      </div>
    </div>
  </div>
    <script src="{{asset('loginform/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('loginform/js/popper.min.js')}}"></script>
    <script src="{{asset('loginform/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('loginform/js/main.js')}}"></script>
  </body>
</html>