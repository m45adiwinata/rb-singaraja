@extends('admin.layouts.main-admin')
@section('style')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Master Users</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Master Users</li>
                </ol>
            </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    @if(Session::has('success'))
        <div class="alert alert-success" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;margin: 1rem 14px;" role="alert">
          <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
        </div>
    @endif
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md">
                    <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Daftar Users</h3>
                        <div class="card-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <a class="btn btn-tools btn-sm" data-toggle="modal" data-target="#modaladduser" ><i class="fas fa-plus-circle"></i> Tambah User</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-bordered">
                        <thead>
                            <tr>
                            <th style="width: 10px">#</th>
                            <th>Nama</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th style="width: 120px">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($users))
                                @foreach($users as $key => $data)
                                <tr>
                                    <td>{{$key+1}}.</td>
                                    <td>{{$data->name}}</td>
                                    <td>{{$data->username}}</td>
                                    <td>{{$data->password}}</td>
                                    <td>
                                        <a class="btn" onclick="user.edit({{$data->id}},'{{$data->name}}','{{$data->username}}')"><i class="fas fa-edit"></i></a>
                                        <button class="btn" onclick="user.delete({{$data->id}},'{{$data->name}}')"><i class="far fa-trash-alt"></i></button>
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td> Data belum tersedia</td>
                                </tr>
                            @endif
                          
                        </tbody>
                        </table>
                    </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
    <!-- /.main content -->
    <!-- Modal Add -->
    <div class="modal fade" id="modaladduser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{route('add-user')}}" id="add-user" enctype="multipart/form-data">
                @csrf
                    <div class="modal-body">
                                <div class="form-group required">
                                    <label for="title" class="control-label">Nama</label>
                                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan nama" autocomplete="off"  required>
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Username</label>
                                    <input class="form-control" rows="3" placeholder="Masukan username" id="username" name="username" required>
                                </div>
                                <div class="form-group required">
                                    <label for="password" class="control-label">Password</label>
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Masukan password" required>
                                </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit"  class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <!-- Modal Edit -->
    <div class="modal fade" id="modaledituser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{route('edit-user')}}" id="edit-user" enctype="multipart/form-data">
                @csrf
                    <div class="modal-body">
                                <input type="hidden" name="id" id="id_user">
                                <div class="form-group">
                                    <label for="nama2">Nama</label>
                                    <input type="text" class="form-control" id="nama2" name="nama" placeholder="Nama" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label for="username2">Username</label>
                                    <input type="text" class="form-control" id="username2" name="username" placeholder="Username" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label for="password2">Password</label>
                                    <input type="password" class="form-control" id="password2" name="password" placeholder="Password | Tidak perlu diubah jika tidak diganti">
                                </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit"  class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

       <!-- Modal Delete -->
       <div class="modal fade" id="modaldeleteuser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Hapus User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{route('delete-user')}}" id="delete-user" enctype="multipart/form-data">
                @csrf
                    <input type="hidden" name="id" id="id_user">
                    <div class="modal-body" id="delete-text"></div>
                    <div id="nama3"></div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Iya, Hapus</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<!-- /.content-wrapper -->
@endsection

@section('script')

<script>

    $('#add-user').click(function(){
                $("form[name='adduser']").validate({
                    rules: {
                        nama : 'required',
                        username : 'required',
                        password : 'required',
                    },
                    messages: {
                        nama: "Mohon masukan nama kategori!",
                        username: "Mohon masukan username!",
                        password: "Mohon masukan password!",

                        },
                    submitHandler: function(form) {
                        form.submit();
                    }
                });
        });

        var user = {
                'edit': function(id, nama,username) {
                    $('#edit-user #id_user').val(id);
                    $('#edit-user #nama2').val(nama);
                    $('#edit-user #username2').val(username);
                    $('#modaledituser').modal('show'); 
                },

                'delete': function(id,nama) {
                    $('#delete-user #delete-text').html('Anda yakin ingin menghapus? '+'<b>'+nama+'</b>');
                    $('#delete-user #id_user').val(id);
                    $('#modaldeleteuser').modal('show'); 
                }
        }
</script>
@endsection