@extends('admin.layouts.main-admin')
@section('style')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Master Kategori Berita</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Master Kategori Berita</li>
                </ol>
            </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    @if(Session::has('success'))
        <div class="alert alert-success" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;margin: 1rem 14px;" role="alert">
          <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
        </div>
    @elseif(Session::has('danger'))
        <div class="alert alert-success" style="padding: 5px 10px;background-color: #eb5757;border-color: #eb5757;color: white;margin: 1rem 14px;" role="alert">
        <i class="fas fa-exclamation" style="margin-right:5px;"></i>{{ Session::get('danger') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
        </div>
    @endif
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md">
                    <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Daftar Kategori</h3>
                        <div class="card-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <a class="btn btn-tools btn-sm" data-toggle="modal" data-target="#modaladdkategori" ><i class="fas fa-plus-circle"></i> Tambah Kategori</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-bordered">
                        <thead>
                            <tr>
                            <th style="width: 10px">#</th>
                            <th>Nama Kateogri</th>
                            <th>Deskripsi</th>
                            <th style="width: 120px">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($categories))
                                @foreach($categories as $key => $category)
                                <tr>
                                    <td>{{$key+1}}.</td>
                                    <td>{{$category->nama}}</td>
                                    <td>{{$category->description}}</td>
                                    <td>
                                        <a class="btn" onclick="categories.edit({{$category->id}},'{{$category->nama}}','{{$category->description}}')"><i class="fas fa-edit"></i></a>
                                        <button class="btn" onclick="categories.delete({{$category->id}})"><i class="far fa-trash-alt"></i></button>
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td> Data belum tersedia</td>
                                </tr>
                            @endif
                          
                        </tbody>
                        </table>
                    </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
    <!-- /.main content -->
    <!-- Modal Add -->
    <div class="modal fade" id="modaladdkategori" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data Kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{route('add-kategori')}}" id="add-kategori" enctype="multipart/form-data">
                @csrf
                    <div class="modal-body">
                                <div class="form-group required">
                                    <label for="title" class="control-label">Nama Kategori</label>
                                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan nama kategori" autocomplete="off"  required>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Deskripsi</label>
                                    <textarea class="form-control" rows="3" placeholder="Masukan deskripsi" id="description" name="description"></textarea>
                                </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit"  class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <!-- Modal Edit -->
    <div class="modal fade" id="modaleditkategori" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data Kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{route('edit-kategori')}}" id="edit-kategori" enctype="multipart/form-data">
                @csrf
                    <div class="modal-body">
                                <input type="hidden" name="id" id="id_kat">
                                <div class="form-group required">
                                    <label for="title" class="control-label">Nama Kategori</label>
                                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan nama kategori" autocomplete="off"  required>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Deskripsi</label>
                                    <textarea class="form-control" rows="3" placeholder="Masukan deskripsi" id="description" name="description"></textarea>
                                </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit"  class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

       <!-- Modal Delete -->
       <div class="modal fade" id="modaldeletekategori" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Hapus Kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{route('delete-kategori')}}" id="delete-kategori" enctype="multipart/form-data">
                @csrf
                    <div class="modal-body" id="delete-text"></div>
                        <input type="hidden" id="id-kat" name="id" >
                        <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Iya, Hapus</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<!-- /.content-wrapper -->
@endsection

@section('script')

<script>

    $('#add-kategori').click(function(){
                $("form[name='addkategori']").validate({
                    rules: {
                        nama : 'required',
                    },
                    messages: {
                        nama: "Mohon masukan nama kategori!",
                        },
                    submitHandler: function(form) {
                        form.submit();
                    }
                });
        });

        var categories = {
                'edit': function(id, nama, description) {
                    $('#edit-kategori #id_kat').val(id);
                    $('#edit-kategori #nama').val(nama);
                    $('#edit-kategori #description').val(description);
                    $('#modaleditkategori').modal('show'); 
                },

                'delete': function(id) {
                    console.log(id);
                    $('#delete-kategori #delete-text').html('Anda yakin ingin menghapus?');
                    $('#delete-kategori #id-kat').val(id);
                    $('#modaldeletekategori').modal('show'); 
                }
        }
</script>
@endsection